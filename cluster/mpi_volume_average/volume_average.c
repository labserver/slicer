#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
	DIR *dp;
	struct dirent *entry;
	struct stat statbuf;
	short int* data;
	float* avg_data;
	int i, index;
	long fsize;

	/* Read directory and find each file. */
	if((dp = opendir(argv[1])) == 0) {
		fprintf(stderr,"cannot open directory: %s\n", argv[1]);
		return 1;
	}
	chdir(argv[1]);
    index = 0;
	while((entry = readdir(dp)) != 0)
	{
		lstat(entry->d_name,&statbuf);
		if(!S_ISDIR(statbuf.st_mode)) {
			/* Found a file, ignore directories */
			printf("Averaging: %s\n",entry->d_name);

			// Open file and read current volume
			FILE *f = fopen(entry->d_name, "rb");
			fseek(f, 0, SEEK_END);
			fsize = ftell(f);
			fseek(f, 0, SEEK_SET);
			if( index == 0 )
			{
				data = (short int*) malloc(fsize);
				avg_data = (float*) calloc(fsize/2, sizeof(float));
				index++;
			}
			fread(data, sizeof(short int), fsize/2, f);
			fclose(f);
			// Average
			for (i=0;i<fsize/2;i++)
			{
				avg_data[i]+=data[i];
			}
		}
	}
	// Normalize and write
	for (i=0;i<fsize/2;i++)
	{
		data[i] = (short int) round(avg_data[i]/(fsize/2));
	}
	FILE* f = fopen("average.bin", "wb");
	fwrite(data, sizeof(short int), fsize/2, f);
	fclose(f);
	chdir("..");
	closedir(dp);
	free(data);
	free(avg_data);
	return 0;
}


