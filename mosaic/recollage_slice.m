function varargout = recollage_slice(varargin)
% RECOLLAGE_SLICE MATLAB code for recollage_slice.fig
%      RECOLLAGE_SLICE, by itself, creates a new RECOLLAGE_SLICE or raises the existing
%      singleton*.
%
%      H = RECOLLAGE_SLICE returns the handle to a new RECOLLAGE_SLICE or the handle to
%      the existing singleton*.
%
%      RECOLLAGE_SLICE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RECOLLAGE_SLICE.M with the given input arguments.
%
%      RECOLLAGE_SLICE('Property','Value',...) creates a new RECOLLAGE_SLICE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before recollage_slice_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to recollage_slice_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help recollage_slice

% Last Modified by GUIDE v2.5 15-Nov-2013 14:15:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @recollage_slice_OpeningFcn, ...
                   'gui_OutputFcn',  @recollage_slice_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before recollage_slice is made visible.
function recollage_slice_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to recollage_slice (see VARARGIN)

% Choose default command line output for recollage_slice
handles.output = hObject;

handles.currentSlice=1;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes recollage_slice wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = recollage_slice_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function eSlices_Callback(hObject, eventdata, handles)
% hObject    handle to eSlices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eSlices as text
%        str2double(get(hObject,'String')) returns contents of eSlices as a double


% --- Executes during object creation, after setting all properties.
function eSlices_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eSlices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ePixels_Callback(hObject, eventdata, handles)
% hObject    handle to ePixels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ePixels as text
%        str2double(get(hObject,'String')) returns contents of ePixels as a double


% --- Executes during object creation, after setting all properties.
function ePixels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ePixels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pLoad.
function pLoad_Callback(hObject, eventdata, handles)
% hObject    handle to pLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.folder = uigetdir;
load([handles.folder '/MemMapFileInfo.mat']);
handles.infox=info.x;
handles.infoy=info.y;
handles.infoz=info.z;
handles.npx=str2double(get(handles.ePixels,'String'));
%sAxes1
set(handles.sAxes1,'Max',handles.npx)
set(handles.sAxes1,'Min',1)
set(handles.sAxes1, 'Value', 1);
set(handles.sAxes1, 'SliderStep', [1/(handles.npx) 1/(handles.npx)]);
%sAxes2
set(handles.sAxes2,'Max',handles.npx)
set(handles.sAxes2,'Min',1)
set(handles.sAxes2, 'Value', 1);
set(handles.sAxes2, 'SliderStep', [1/(handles.npx-1) 1/(handles.npx-1)]);
% Slice Volume Pre and Post
m=memmapfile([handles.folder '/MemMapFile.mat'], 'Format', {'uint16' [info.x info.y info.z] 'x'});
handles.m=m;
pre = handles.m.Data(1).x(:,:,1:handles.npx);
post = handles.m.Data(1).x(:,:,handles.npx+1:2*handles.npx);

axes(handles.axes1)
imagesc(squeeze(pre(:,:,1))); colormap gray;axis off;axis image
axes(handles.axes2)
imagesc(squeeze(post(:,:,1))); colormap gray;axis off;axis image


set(handles.tSlices,'String', num2str(str2double(get(handles.eSlices,'String'))));
set(handles.tSliceview1,'String', num2str(1));
set(handles.tSliceview2,'String', num2str(2));
% Update handles structure
guidata(hObject, handles);



% --- Executes on slider movement.
function sAxes1_Callback(hObject, eventdata, handles)
% hObject    handle to sAxes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tSlice1,'String', num2str(round(get(handles.sAxes1,'Value'))));
pre = handles.m.Data(1).x(:,:,1+handles.npx*(handles.currentSlice-1):handles.npx*(handles.currentSlice));

axes(handles.axes1)
    imagesc(squeeze(pre(:,:,round(get(handles.sAxes1,'Value'))))); colormap gray;axis off;axis image
%imagesc(squeeze(pre(:,:,num2str(round(get(handles.sAxes1,'Value')))))); colormap gray;axis off;axis image


% --- Executes during object creation, after setting all properties.
function sAxes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sAxes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on slider movement.
function sAxes2_Callback(hObject, eventdata, handles)
% hObject    handle to sAxes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tSlice2,'String', num2str(round(get(handles.sAxes2,'Value'))));
post = handles.m.Data(1).x(:,:,1+handles.npx*(handles.currentSlice):handles.npx*(1+handles.currentSlice));
axes(handles.axes2)
    imagesc(squeeze(post(:,:,round(get(handles.sAxes2,'Value'))))); colormap gray;axis off;axis image

% --- Executes during object creation, after setting all properties.
function sAxes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sAxes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pOK.
function pOK_Callback(hObject, eventdata, handles)
% hObject    handle to pOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.cp=0;
pre=zeros(size(handles.m.Data(1).x(:,:,1),1)+200,size(handles.m.Data(1).x(:,:,1),2)+200);
pre(101:100+size(handles.m.Data(1).x(:,:,1),1),101:100+size(handles.m.Data(1).x(:,:,1),2))=handles.m.Data(1).x(:,:,round(get(handles.sAxes1,'Value'))+handles.npx*(handles.currentSlice-1));
handles.pre=pre;
post=zeros(size(handles.m.Data(1).x(:,:,1),1)+200,size(handles.m.Data(1).x(:,:,1),2)+200);
post(101:100+size(handles.m.Data(1).x(:,:,1),1),101:100+size(handles.m.Data(1).x(:,:,1),2))=handles.m.Data(1).x(:,:,round(get(handles.sAxes2,'Value'))+handles.npx*(handles.currentSlice));
handles.post=post;
axes(handles.axes3)
imshow(pre,'DisplayRange',[1 65536]);axis off;axis image
%green=cat(3,zeros(size(pre)),ones(size(pre)),zeros(size(pre)));
red=cat(3,ones(size(post)),zeros(size(post)),zeros(size(post)));
hold on
r=imshow(red);
%g=imshow(green);
hold off
%pre=pre./65536;
post=post./65536;
%set(g, 'AlphaData',pre);
set(r,'AlphaData',post);

%sShiftY
set(handles.sShiftY,'Max',100)
set(handles.sShiftY,'Min',-100)
set(handles.sShiftY, 'Value', 0);
set(handles.sShiftY, 'SliderStep', [1/200 1/200]);
set(handles.tShiftY,'String', num2str(0));
%sShiftX
set(handles.sShiftX,'Max',100)
set(handles.sShiftX,'Min',-100)
set(handles.sShiftX, 'Value', 0);
set(handles.sShiftX, 'SliderStep', [1/200 1/200]);
set(handles.tShiftX,'String', num2str(0));

handles.x=0;
handles.y=0;
set(handles.tCurrentPointX, 'String', num2str(0));
set(handles.tCurrentPointY, 'String', num2str(0));
set(handles.tZoom, 'String', num2str(1));
    f =get(handles.axes3,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
% Update handles structure
guidata(hObject, handles);


% --- Executes on slider movement.
function sShiftY_Callback(hObject, eventdata, handles)
% hObject    handle to sShiftY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tShiftY,'String', num2str(round(get(handles.sShiftY,'Value'))));
handles.y=get(handles.sShiftY,'Value');

post=zeros(size(handles.m.Data(1).x(:,:,1),1)+200,size(handles.m.Data(1).x(:,:,1),2)+200);
post(101-handles.x:-handles.x+100+size(handles.m.Data(1).x(:,:,1),1),101+handles.y:handles.y+100+size(handles.m.Data(1).x(:,:,1),2))=handles.m.Data(1).x(:,:,round(get(handles.sAxes2,'Value'))+handles.npx*(handles.currentSlice));
handles.post=post;
axes(handles.axes3)
imshow(handles.pre,'DisplayRange',[1 65536]);axis off;axis image
red=cat(3,ones(size(post)),zeros(size(post)),zeros(size(post)));
hold on
r=imshow(red);
hold off
post=post./65536;
set(r,'AlphaData',post);
    f =get(handles.axes3,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function sShiftY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sShiftY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sShiftX_Callback(hObject, eventData, handles)
% hObject    handle to sShiftX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tShiftX,'String', num2str(round(get(handles.sShiftX,'Value'))));
handles.x=get(handles.sShiftX,'Value');
% cp = get(handles.axes3,'CurrentPoint');
% y=get(gca,'YLim');
% x=get(gca,'XLim');
% z=round(handles.infox/y(2));
% cp(1)=cp(1)*z;
% cp(3)=cp(3)*z;
% handles.pre=handles.pre(min(max(round(cp(1)-size(handles.pre,1)/(z*2)),1),round(size(handles.pre,1)/z)):min(max(round(cp(1)+size(handles.pre,1)/(z*2)),round((size(handles.pre,1)+1)/z)),size(handles.pre,1)),...
%     min(max(round(cp(3)-size(handles.pre,2)/(z*2)),1),round(size(handles.pre,2)/z)):min(max(round(cp(3)+size(handles.pre,2)/(z*2)),round((1+size(handles.pre,2))/z)),size(handles.pre,2)));
% handles.post=handles.post(min(max(round(cp(1)-size(handles.post,1)/(z*2)),1),round(size(handles.post,1)/z))-handles.x:-handles.x+min(max(round(cp(1)+size(handles.post,1)/(z*2)),round((size(handles.post,1)+1)/z)),size(handles.post,1)),...
%     min(max(round(cp(3)-size(handles.post,2)/(z*2)),1),round(size(handles.post,2)/z))+handles.y:handles.y+min(max(round(cp(3)+size(handles.post,2)/(z*2)),round(size(handles.post,2)+1)/z),size(handles.post,2)));

post=zeros(size(handles.m.Data(1).x(:,:,1),1)+200,size(handles.m.Data(1).x(:,:,1),2)+200);
post(101-handles.x:-handles.x+100+size(handles.m.Data(1).x(:,:,1),1),101+handles.y:handles.y+100+size(handles.m.Data(1).x(:,:,1),2))=handles.m.Data(1).x(:,:,round(get(handles.sAxes2,'Value'))+handles.npx*(handles.currentSlice));
handles.post=post;
axes(handles.axes3)
imshow(handles.pre,'DisplayRange',[1 65536]);axis off;axis image
red=cat(3,ones(size(handles.post)),zeros(size(handles.post)),zeros(size(handles.post)));
hold on
r=imshow(red);
hold off

set(r,'AlphaData',handles.post/65536);
    f =get(handles.axes3,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
 % Update handles structure
 guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function sShiftX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sShiftX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function getpoints(hObject,evnt,handles,which_axe)

cp = get(handles.axes3,'CurrentPoint');

handles.pre=handles.pre(min(max(round(cp(3)-size(handles.pre,1)/4),1),round(size(handles.pre,1)/2)):min(max(round(cp(3)+size(handles.pre,1)/4),round((size(handles.pre,1)+1)/2)),size(handles.pre,1)),...
    min(max(round(cp(1)-size(handles.pre,2)/4),1),round(size(handles.pre,2)/2)):min(max(round(cp(1)+size(handles.pre,2)/4),round((1+size(handles.pre,2))/2)),size(handles.pre,2)));
handles.post=handles.post(min(max(round(cp(3)-size(handles.post,1)/4),1),round(size(handles.post,1)/2)):min(max(round(cp(3)+size(handles.post,1)/4),round((size(handles.post,1)+1)/2)),size(handles.post,1)),...
    min(max(round(cp(1)-size(handles.post,2)/4),1),round(size(handles.post,2)/2)):min(max(round(cp(1)+size(handles.post,2)/4),round(size(handles.post,2)+1)/2),size(handles.post,2)));


axes(handles.axes3)
imshow(handles.pre,'DisplayRange',[1 65536]);axis off;axis image
red=cat(3,ones(size(handles.post)),zeros(size(handles.post)),zeros(size(handles.post)));
hold on
r=imshow(red);
hold off

set(r,'AlphaData',handles.post./65535);


f =get(handles.axes3,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})







                      
    

% --- Executes on button press in pNext.
function pNext_Callback(hObject, eventdata, handles)
% hObject    handle to pNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Slice Volume Pre and Post
if handles.currentSlice < str2double(get(handles.eSlices,'String'))-1

x=get(handles.sShiftX, 'Value');
y=get(handles.sShiftY, 'Value');
z=round(get(handles.sAxes2, 'Value'))+40-round(get(handles.sAxes1, 'Value'));
handles.delta(:,handles.currentSlice)=[x y z];
set(handles.sAxes1, 'Value', 1);
set(handles.sAxes2, 'Value', 1);
set(handles.tSlice1,'String', num2str(1));
set(handles.tSlice2,'String', num2str(1));

handles.currentSlice=handles.currentSlice+1;
set(handles.tSliceview1,'String', num2str(handles.currentSlice));
set(handles.tSliceview2,'String', num2str(handles.currentSlice+1));
pre = handles.m.Data(1).x(:,:,1+handles.npx*(handles.currentSlice-1):handles.npx*(handles.currentSlice));
post = handles.m.Data(1).x(:,:,1+handles.npx*(handles.currentSlice):handles.npx*(1+handles.currentSlice));

axes(handles.axes1)
imagesc(squeeze(pre(:,:,1))); colormap gray;axis off;axis image
axes(handles.axes2)
imagesc(squeeze(post(:,:,1))); colormap gray;axis off;axis image
cla(handles.axes3)

elseif handles.currentSlice == str2double(get(handles.eSlices,'String'))-1
    
x=get(handles.sShiftX, 'Value');
y=get(handles.sShiftY, 'Value');
z=round(get(handles.sAxes2, 'Value'))+40-round(get(handles.sAxes1, 'Value'));
handles.delta(:,handles.currentSlice)=[x y z]
delta = hanldes.delta;
    save([handles.folder '/deltaM.mat'], 'delta')
    close all
end



% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pZoomOut.
function pZoomOut_Callback(hObject, eventdata, handles)
% hObject    handle to pZoomOut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
post=zeros(size(handles.m.Data(1).x(:,:,1),1)+200,size(handles.m.Data(1).x(:,:,1),2)+200);
post(101-handles.x:-handles.x+100+size(handles.m.Data(1).x(:,:,1),1),101+handles.y:handles.y+100+size(handles.m.Data(1).x(:,:,1),2))=handles.m.Data(1).x(:,:,round(get(handles.sAxes2,'Value'))+handles.npx*(handles.currentSlice));
handles.post=post;
axes(handles.axes3)
imshow(handles.pre,'DisplayRange',[1 65536]);axis off;axis image
red=cat(3,ones(size(handles.post)),zeros(size(handles.post)),zeros(size(handles.post)));
hold on
r=imshow(red);
hold off

set(r,'AlphaData',handles.post/65536);
    f =get(handles.axes3,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
 % Update handles structure
 guidata(hObject, handles);