function [a] = countour(nifti)
a=ones(size(nifti));
GMAG=zeros(size(nifti));
for k=1:size(nifti,3)
[Gmag,Gdir] = imgradient(squeeze(nifti(:,:,k)));
GMAG(:,:,k) = Gmag;
end
a(GMAG<20)=0;

% for i=1:size(Gmag,1)
%     for j=1:size(Gmag,2)
%         if Gmag(i,j)<20;
%             a(i,j,k)=0;
%         else
%             a(i,j,k)=1;
%         end
%     end
% end
%end

