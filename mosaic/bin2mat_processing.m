function bin2mat_processing (directory, frameX, frameY, frameZ, nAveraging)
%fname : directory and file name

%Reference
fid=fopen([directory 'reference.bin'],'rb');
ref = fread(fid, inf, 'uint16');
ref = reshape(ref,[1152 512]);

% Filter
filter = blackman(4*1152,'symmetric');
filter = filter(end/2-575:end/2+576);

% Volume moyen
sumV = zeros(150,512,512);

for z = 1:frameZ
    for y = 1:frameY
        for x = 1:frameX
            for n = 1: nAveraging
            tic
            %fid = fopen([directory 'RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.bin'], 'rb');
            fid = fopen([directory 'RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '(' sprintf( '%01.0f', n) ')' '.bin'], 'rb');
            img = fread(fid, inf, 'uint16');
            img = reshape(img,[1152 512 512]);
            %save([directory '/RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'img','-v7.3');

            
            img = img - repmat(mean(ref,2), [1 512 512]);
            img = img.*repmat(filter,[1 512 512]);
            fftimg = fft(img,[],1);
            clear img
            fftimg = (abs(fftimg(1:150,:,:)));
            noise_floor = mean(mean(fftimg(round((1-0.05)*end):end,:)));
            fftimg = 10*log10(fftimg / noise_floor);
            fftimg(fftimg<0) = 0;
            FFTIMG(:,:,:,n) = fftimg;
            clear fftimg
            end
            FFTIMG = mean(FFTIMG,4);
           % fid = fopen(['/home/vibratome/CerveauSouris/volume/' 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'], 'w');
            %fwrite(fid,FFTIMG, 'double');
            save([directory 'volume/' 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'FFTIMG');
            F = (x-1) + frameX*(y-1) + frameX*frameY*(z-1);
            sumV = (F*(sumV)+FFTIMG)/(F+1);
            clear FFTIMG
            fclose('all');
            %delete([directory '/RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.bin']);
            toc
        end
    end
end
save([directory 'volume/meanVolume.mat'],'sumV')
%save([directory 'meanVolume.mat'],'sumV');