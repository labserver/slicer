function varargout = slicer_view(varargin)
% SLICER_VIEW MATLAB code for slicer_view.fig
%      SLICER_VIEW, by itself, creates a new SLICER_VIEW or raises the existing
%      singleton*.
%
%      H = SLICER_VIEW returns the handle to a new SLICER_VIEW or the handle to
%      the existing singleton*.
%
%      SLICER_VIEW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SLICER_VIEW.M with the given input arguments.
%
%      SLICER_VIEW('Property','Value',...) creates a new SLICER_VIEW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before slicer_view_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to slicer_view_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help slicer_view

% Last Modified by GUIDE v2.5 22-Jan-2014 15:28:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @slicer_view_OpeningFcn, ...
    'gui_OutputFcn',  @slicer_view_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before slicer_view is made visible.
function slicer_view_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to slicer_view (see VARARGIN)

% Choose default command line output for slicer_view
handles.output = hObject;


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes slicer_view wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%slider1
set(handles.slider1,'Max',512)
set(handles.slider1,'Min',1)
set(handles.slider1, 'Value', 256);
set(handles.slider1, 'SliderStep', [1/511 1/511]);
%slider2
set(handles.slider2,'Max',512)
set(handles.slider2,'Min',1)
set(handles.slider2, 'Value', 256);
set(handles.slider2, 'SliderStep', [1/511 1/511]);
%slider3
set(handles.slider3,'Max',512)
set(handles.slider3,'Min',1)
set(handles.slider3, 'Value', 256);
set(handles.slider3, 'SliderStep', [1/511 1/511]);



% --- Outputs from this function are returned to the command line.
function varargout = slicer_view_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_pushbutton.
function load_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to load_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read folder with multiple slices
handles.folder = uigetdir;
handles.zoom = 0;
load([handles.folder '/deltaM.mat'])
load([handles.folder '/MemMapFileInfo.mat']);
handles.infox=info.x+300;
handles.infoy=info.y+300;
handles.infoz=info.z-sum(round(delta(3,:)))-size(delta,2);%+1+1;
if(handles.folder)
    
    % Verify if reduced version is already computed
    tmp=dir([handles.folder '/vol_reduced.mat']);
    if(size(tmp,1)==0)
        % Need to do the image subsampling (careful with ordering)
        f=memmapfile([handles.folder '/final.mat'], 'Format', {'uint16' [handles.infox handles.infoy handles.infoz] 'x'}, 'Writable', true);
        tmp_volume=zeros(512,512,handles.infoz);
        for i=1:handles.infoz
            big_img=squeeze(f.Data(1).x(:,:,i));
            tmp_volume(:,:,i) = imresize(big_img,[512,512]);
        end
        % We reduced in x and y, need to reduce in z...
        volume=zeros(512,512,512);
        for i=1:512
            volume(i,:,:)=imresize(squeeze(tmp_volume(i,:,:)),[512,512]);
        end
        clear tmp_volume;
        save([handles.folder '/vol_reduced.mat'],'volume');
    else
        % Load Reduced version
        load([handles.folder '/vol_reduced.mat']);
    end
    handles.volume=volume;
    % LOAD NIFTI
    nifti=load_nii('/home/vibratome/pouf.nii');
        tmp_volume=zeros(512,512,282);
        for i=1:282
            big_img=squeeze(nifti.img(:,:,i));
            tmp_volume(:,:,i) = imresize(big_img,[512,512]);
        end
        volume=zeros(512,512,512);
        for i=1:512
            volume(i,:,:)=imresize(squeeze(tmp_volume(i,:,:)),[512,512]);
        end
        clear tmp_volume;
  
    handles.nifti=volume;
    handles.bs = 0;
    % Update display
    axes(handles.axes1)
    
    imagesc(squeeze(handles.volume(:,:,end/2))); colormap 'gray'
    
%     b=rot90(squeeze(handles.nifti(:,end/2,:)));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);
    
    set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
    axis off;
    f =get(handles.axes1,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,1})
    axes(handles.axes2)
    imagesc(squeeze(handles.volume(:,end/2,:))); colormap 'gray'
    
%     b=fliplr(rot90(squeeze(handles.nifti(end/2,:,:))));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);
    
    set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
    axis off;
    f =get(handles.axes2,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,2})
    axes(handles.axes3)
    imagesc(rot90(rot90(rot90(squeeze(handles.volume(end/2,:,:)))))); colormap 'gray'
    
%     b=rot90(squeeze(handles.nifti(:,:,end/2)));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);
    
    set(gca,'DataAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infoy/handles.infoy 1])
    axis off;
    f =get(handles.axes3,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
    
    % Update handles structure
    guidata(hObject, handles);
end




% --- Executes on button press in filter_pushbutton.
function filter_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to filter_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function getpoints(src,evnt,handles,which_axe)
load([handles.folder '/MemMapFileInfo.mat']);

f=memmapfile([handles.folder '/final.mat'], 'Format', {'uint16' [handles.infox handles.infoy handles.infoz] 'x'}, 'Writable', true);
if get(handles.tzoom2,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.axes1,'CurrentPoint');
            axes(handles.axes1)
            imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/4),1),round(1*512/2)),min(max(round(cp(3)-512/4),1),round(1*512/2)), round(512/2), round(512/2)], 'EdgeColor', [1 0 0])
            axis off;
            
            
            axes(handles.axes4)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/4)*handles.infox/512),1),round(1*handles.infox/2)):min(max(round((cp(3)+512/4)*handles.infox/512),round(handles.infox/2+1)),round(handles.infox)),...
                min(max(round((cp(1)-512/4)*handles.infoy/512),1),round(1*handles.infoy/2)):min(max(round((cp(1)+512/4)*handles.infoy/512),round(handles.infoy/2+1)),round(handles.infoy)),...
                round(get(handles.slider1,'Value')*handles.infoz/512))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes1,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            
        case 2
            cp = get(handles.axes2,'CurrentPoint');
            axes(handles.axes2)
            imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/4),1),round(1*512/2)),min(max(round(cp(3)-512/4),1),round(1*512/2)), round(512/2), round(512/2)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes5)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/4)*handles.infox/512),1),round(1*handles.infox/2)):min(max(round((cp(3)+512/4)*handles.infox/512),round(handles.infox/2+1)),round(handles.infox)),...
                round(get(handles.slider2,'Value')*handles.infoy/512),...
                min(max(round((cp(1)-512/4)*handles.infoz/512),1),round(1*handles.infoz/2)):min(max(round((cp(1)+512/4)*handles.infoz/512),round(handles.infoz/2+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes2,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})
        case 3
            cp = get(handles.axes3,'CurrentPoint');
            axes(handles.axes3)
            imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));
            set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/4),1),round(1*512/2)),min(max(round(cp(3)-512/4),1),round(1*512/2)), round(512/2), round(512/2)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes6)
            imagesc(squeeze(f.Data(1).x(round(get(handles.slider3,'Value')*handles.infox/512),...
                min(max(round((cp(3)-512/4)*handles.infoy/512),1),round(1*handles.infoy/2)):min(max(round((cp(3)+512/4)*handles.infoy/512),round(handles.infoy/2+1)),round(handles.infoy)),...
                min(max(round((cp(1)-512/4)*handles.infoz/512),1),round(1*handles.infoz/2)):min(max(round((cp(1)+512/4)*handles.infoz/512),round(handles.infoz/2+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infoy/handles.infoy 1])
            axis off
            f =get(handles.axes3,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
            
    end
    
elseif get(handles.tzoom5,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.axes1,'CurrentPoint');
            axes(handles.axes1)
            imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/10),1),round(4*512/5)),min(max(round(cp(3)-512/10),1),round(4*512/5)), round(512/5), round(512/5)], 'EdgeColor', [1 0 0])
            axis off;
            
            
            axes(handles.axes4)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/10)*handles.infox/512),1),round(4*handles.infox/5)):min(max(round((cp(3)+512/10)*handles.infox/512),round(handles.infox/5+1)),round(handles.infox)),...
                min(max(round((cp(1)-512/10)*handles.infoy/512),1),round(4*handles.infoy/5)):min(max(round((cp(1)+512/10)*handles.infoy/512),round(handles.infoy/5+1)),round(handles.infoy)),...
                round(get(handles.slider1,'Value')*handles.infoz/512))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes1,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            
        case 2
            cp = get(handles.axes2,'CurrentPoint');
            axes(handles.axes2)
            imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/10),1),round(4*512/5)),min(max(round(cp(3)-512/10),1),round(4*512/5)), round(512/5), round(512/5)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes5)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/10)*handles.infox/512),1),round(4*handles.infox/5)):min(max(round((cp(3)+512/10)*handles.infox/512),round(handles.infox/5+1)),round(handles.infox)),...
                round(get(handles.slider2,'Value')*handles.infoy/512),...
                min(max(round((cp(1)-512/10)*handles.infoz/512),1),round(4*handles.infoz/5)):min(max(round((cp(1)+512/10)*handles.infoz/512),round(handles.infoz/5+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes2,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})
        case 3
            cp = get(handles.axes3,'CurrentPoint');
            axes(handles.axes3)
            imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));
            set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/10),1),round(4*512/5)),min(max(round(cp(3)-512/10),1),round(4*512/5)), round(512/5), round(512/5)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes6)
            imagesc(squeeze(f.Data(1).x(round(get(handles.slider3,'Value')*handles.infox/512),...
                min(max(round((cp(3)-512/10)*handles.infoy/512),1),round(4*handles.infoy/5)):min(max(round((cp(3)+512/10)*handles.infoy/512),round(handles.infoy/5+1)),round(handles.infoy)),...
                min(max(round((cp(1)-512/10)*handles.infoz/512),1),round(4*handles.infoz/5)):min(max(round((cp(1)+512/10)*handles.infoz/512),round(handles.infoz/5+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infoy/handles.infoy 1])
            axis off
            f =get(handles.axes3,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
            
    end
elseif get(handles.tzoom10,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.axes1,'CurrentPoint');
            axes(handles.axes1)
            imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/20),1),round(9*512/10)),min(max(round(cp(3)-512/20),1),round(9*512/10)), round(512/10), round(512/10)], 'EdgeColor', [1 0 0])
            axis off;
            
            
            axes(handles.axes4)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/20)*handles.infox/512),1),round(9*handles.infox/10)):min(max(round((cp(3)+512/20)*handles.infox/512),round(handles.infox/10+1)),round(handles.infox)),...
                min(max(round((cp(1)-512/20)*handles.infoy/512),1),round(9*handles.infoy/10)):min(max(round((cp(1)+512/20)*handles.infoy/512),round(handles.infoy/10+1)),round(handles.infoy)),...
                round(get(handles.slider1,'Value')*handles.infoz/512))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes1,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
        case 2
            cp = get(handles.axes2,'CurrentPoint');
            axes(handles.axes2)
            imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/20),1),round(9*512/10)),min(max(round(cp(3)-512/20),1),round(9*512/10)), round(512/10), round(512/10)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes5)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/20)*handles.infox/512),1),round(9*handles.infox/10)):min(max(round((cp(3)+512/20)*handles.infox/512),round(handles.infox/10+1)),round(handles.infox)),...
                round(get(handles.slider2,'Value')*handles.infoy/512),...
                min(max(round((cp(1)-512/20)*handles.infoz/512),1),round(9*handles.infoz/10)):min(max(round((cp(1)+512/20)*handles.infoz/512),round(handles.infoz/10+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes2,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})
        case 3
            cp = get(handles.axes3,'CurrentPoint');
            axes(handles.axes3)
            imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));
            set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/20),1),round(9*512/10)),min(max(round(cp(3)-512/20),1),round(9*512/10)), round(512/10), round(512/10)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes6)
            imagesc(squeeze(f.Data(1).x(round(get(handles.slider3,'Value')*handles.infox/512),...
                min(max(round((cp(3)-512/20)*handles.infoy/512),1),round(9*handles.infoy/10)):min(max(round((cp(3)+512/20)*handles.infoy/512),round(handles.infoy/10+1)),round(handles.infoy)),...
                min(max(round((cp(1)-512/20)*handles.infoz/512),1),round(9*handles.infoz/10)):min(max(round((cp(1)+512/20)*handles.infoz/512),round(handles.infoz/10+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infoy/handles.infoy 1])
            axis off
            f =get(handles.axes3,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
    end
elseif get(handles.tzoom20,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.axes1,'CurrentPoint');
            axes(handles.axes1)
            imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/40),1),round(19*512/20)),min(max(round(cp(3)-512/40),1),round(19*512/20)), round(512/20), round(512/20)], 'EdgeColor', [1 0 0])
            axis off;
            
            
            axes(handles.axes4)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/40)*handles.infox/512),1),round(19*handles.infox/20)):min(max(round((cp(3)+512/40)*handles.infox/512),round(handles.infox/20+1)),round(handles.infox)),...
                min(max(round((cp(1)-512/40)*handles.infoy/512),1),round(19*handles.infoy/20)):min(max(round((cp(1)+512/40)*handles.infoy/512),round(handles.infoy/20+1)),round(handles.infoy)),...
                round(get(handles.slider1,'Value')*handles.infoz/512))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes1,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
        case 2
            cp = get(handles.axes2,'CurrentPoint');
            axes(handles.axes2)
            imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/40),1),round(19*512/20)),min(max(round(cp(3)-512/40),1),round(19*512/20)), round(512/20), round(512/20)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes5)
            imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-512/40)*handles.infox/512),1),round(19*handles.infox/20)):min(max(round((cp(3)+512/40)*handles.infox/512),round(handles.infox/20+1)),round(handles.infox)),...
                round(get(handles.slider2,'Value')*handles.infoy/512),...
                min(max(round((cp(1)-512/40)*handles.infoz/512),1),round(19*handles.infoz/20)):min(max(round((cp(1)+512/40)*handles.infoz/512),round(handles.infoz/20+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.axes2,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})
        case 3
            cp = get(handles.axes3,'CurrentPoint');
            axes(handles.axes3)
            imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));
            set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            rectangle('Position',[min(max(round(cp(1)-512/40),1),round(19*512/20)),min(max(round(cp(3)-512/40),1),round(19*512/20)), round(512/20), round(512/20)], 'EdgeColor', [1 0 0])
            axis off;
            axes(handles.axes6)
            imagesc(squeeze(f.Data(1).x(round(get(handles.slider3,'Value')*handles.infox/512),...
                min(max(round((cp(3)-512/40)*handles.infoy/512),1),round(19*handles.infoy/20)):min(max(round((cp(3)+512/40)*handles.infoy/512),round(handles.infoy/20+1)),round(handles.infoy)),...
                min(max(round((cp(1)-512/40)*handles.infoz/512),1),round(19*handles.infoz/20)):min(max(round((cp(1)+512/40)*handles.infoz/512),round(handles.infoz/20+1)),round(handles.infoz)))));
            set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infoy/handles.infoy 1])
            axis off
            f =get(handles.axes3,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
    end
else
    switch(which_axe)
        
        case 1
            cp = get(handles.axes1,'CurrentPoint');
            axes(handles.axes1)
            imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
            line([1:512],cp(3), 'Color', [1 0 0])
            line(cp(1),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes1,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            axes(handles.axes2)
            imagesc(squeeze(handles.volume(:,round(cp(1)),:)));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            line([1:512],cp(3), 'Color', [1 0 0])
            line(round(get(handles.slider1,'Value')),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes2,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})
            axes(handles.axes3)
            imagesc(squeeze(handles.volume(round(cp(3)),:,:)));
            set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            line([1:512],cp(1), 'Color', [1 0 0])
            line(round(get(handles.slider1,'Value')),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes3,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
            set(handles.slider2, 'Value', round(cp(1)));
            set(handles.text2,'String', num2str(round(cp(1))));
            set(handles.slider3, 'Value', round(cp(3)));
            set(handles.text3,'String', num2str(round(cp(3))));
            
            
            
        case 2
            cp = get(handles.axes2,'CurrentPoint');
            axes(handles.axes2)
            imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            line([1:512],cp(3), 'Color', [1 0 0])
            line(cp(1),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes2,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})
            axes(handles.axes1)
            imagesc(squeeze(handles.volume(:,:,round(cp(1)))));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
            line([1:512],cp(3), 'Color', [1 0 0])
            line(round(get(handles.slider2,'Value')),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes1,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            axes(handles.axes3)
            imagesc(squeeze(handles.volume(round(cp(3)),:,:)));
            set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            line([1:512],round(get(handles.slider2,'Value')), 'Color', [1 0 0])
            line(cp(1),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes3,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
            set(handles.slider1, 'Value', round(cp(1)));
            set(handles.text1,'String', num2str(round(cp(1))));
            set(handles.slider3, 'Value', round(cp(3)));
            set(handles.text3,'String', num2str(round(cp(3))));
            
            
        case 3
            cp = get(handles.axes3,'CurrentPoint');
            axes(handles.axes3)
            imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));
            set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            line([1:512],cp(3), 'Color', [1 0 0])
            line(cp(1),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes3,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
            axes(handles.axes1)
            imagesc(squeeze(handles.volume(:,:,round(cp(1)))));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
            line([1:512],round(get(handles.slider3,'Value')), 'Color', [1 0 0])
            line(cp(3),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes1,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            axes(handles.axes2)
            imagesc(squeeze(handles.volume(:,round(cp(3)),:)));
            set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            line([1:512],round(get(handles.slider3,'Value')), 'Color', [1 0 0])
            line(cp(1),[1:512], 'Color', [1 0 0])
            axis off;
            f =get(handles.axes2,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})
            set(handles.slider1, 'Value', round(cp(1)));
            set(handles.text1,'String', num2str(round(cp(1))));
            set(handles.slider2, 'Value', round(cp(3)));
            set(handles.text2,'String', num2str(round(cp(3))));
    end
end


function big_to_small(ix,iy,iz)
% This function maps the dimension from the reduced to the big view



% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
load([handles.folder '/MemMapFileInfo.mat']);

set(handles.text1,'String', num2str(round(get(handles.slider1,'Value'))));
axes(handles.axes1)
imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));

%     b=rot90(squeeze(handles.nifti(:,round(get(handles.slider1,'Value')),:)));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
axis off;
f =get(handles.axes1,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})

axes(handles.axes2)
imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));

%     b=fliplr(rot90(squeeze(handles.nifti(round(get(handles.slider2,'Value')),:,:))));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
line(round(get(handles.slider1,'Value')),[1:512], 'Color', [1 0 0])
axis off;
f =get(handles.axes2,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})

axes(handles.axes3)
imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));

%     b=rot90(squeeze(handles.nifti(:,:,round(get(handles.slider3,'Value')))));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);
    
set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
line(round(get(handles.slider1,'Value')),[1:512], 'Color', [1 0 0])
axis off;
f =get(handles.axes3,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
% set(handles.slider1,'Max',512)
% set(handles.slider1,'Min',1)



% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
load([handles.folder '/MemMapFileInfo.mat']);

set(handles.text2,'String', num2str(round(get(handles.slider2,'Value'))));
axes(handles.axes2)
imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));

%     b=fliplr(rot90(squeeze(handles.nifti(round(get(handles.slider2,'Value')),:,:))));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
axis off;
f =get(handles.axes2,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})
axes(handles.axes1)
imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));

%     b=rot90(squeeze(handles.nifti(:,round(get(handles.slider1,'Value')),:)));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
line(round(get(handles.slider2,'Value')),[1:512], 'Color', [1 0 0])
axis off;
f =get(handles.axes1,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})
axes(handles.axes3)
imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));

%     b=rot90(squeeze(handles.nifti(:,:,round(get(handles.slider3,'Value')))));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
line([1:512],round(get(handles.slider2,'Value')), 'Color', [1 0 0])
axis off;
f =get(handles.axes3,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
load([handles.folder '/MemMapFileInfo.mat']);

set(handles.text3,'String', num2str(round(get(handles.slider3,'Value'))));
axes(handles.axes3)
imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));

%     b=rot90(squeeze(handles.nifti(:,:,round(get(handles.slider3,'Value')))));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
axis off;
f =get(handles.axes3,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})
axes(handles.axes1)
imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));

%     b=rot90(squeeze(handles.nifti(:,round(get(handles.slider1,'Value')),:)));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
line([1:512],round(get(handles.slider3,'Value')), 'Color', [1 0 0])
axis off;
f =get(handles.axes1,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})
axes(handles.axes2)
imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));

%     b=fliplr(rot90(squeeze(handles.nifti(round(get(handles.slider2,'Value')),:,:))));
%     a= countour(b);
%     blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
%     hold on
%     b=imshow(blue);
%     hold off
%     set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
line([1:512],round(get(handles.slider3,'Value')), 'Color', [1 0 0])
axis off;
f =get(handles.axes2,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})



% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function text1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



% --- Executes on button press in tzoom2.
function tzoom2_Callback(hObject, eventdata, handles)
% hObject    handle to tzoom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tzoom2
set(handles.tzoom5,'Value',0)
set(handles.tzoom10,'Value',0)
set(handles.tzoom20,'Value',0)

function tzoom2_CreateFcn(hObject, eventdata, handles)

% --- Executes on button press in tzoom5.
function tzoom5_Callback(hObject, eventdata, handles)
% hObject    handle to tzoom5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tzoom5
set(handles.tzoom2,'Value',0)
set(handles.tzoom10,'Value',0)
set(handles.tzoom20,'Value',0)

function tzoom5_CreateFcn(hObject, eventdata, handles)

% --- Executes on button press in tzoom10.
function tzoom10_Callback(hObject, eventdata, handles)
% hObject    handle to tzoom10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tzoom10
set(handles.tzoom2,'Value',0)
set(handles.tzoom5,'Value',0)
set(handles.tzoom20,'Value',0)

function tzoom10_CreateFcn(hObject, eventdata, handles)

% --- Executes on button press in tzoom20.
function tzoom20_Callback(hObject, eventdata, handles)
% hObject    handle to tzoom20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tzoom20
set(handles.tzoom2,'Value',0)
set(handles.tzoom5,'Value',0)
set(handles.tzoom10,'Value',0)

function tzoom20_CreateFcn(hObject, eventdata, handles)


% --- Executes on button press in pAtlas.
function pAtlas_Callback(hObject, eventdata, handles)
% hObject    handle to pAtlas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load([handles.folder '/MemMapFileInfo.mat']);

set(handles.text2,'String', num2str(round(get(handles.slider2,'Value'))));
axes(handles.axes2)
imagesc(squeeze(handles.volume(:,round(get(handles.slider2,'Value')),:)));

    b=fliplr(rot90(squeeze(handles.nifti(round(get(handles.slider2,'Value')),:,:))));
    a= countour(b);
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    hold on
    b=imshow(blue);
    hold off
    set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
axis off;
f =get(handles.axes2,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})
axes(handles.axes1)
imagesc(squeeze(handles.volume(:,:,round(get(handles.slider1,'Value')))));

    b=rot90(squeeze(handles.nifti(:,512-round(get(handles.slider1,'Value')),:)));
    a= countour(b);
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    hold on
    b=imshow(blue);
    hold off
    set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infox/handles.infoy handles.infoy/handles.infoy 1])
line(round(get(handles.slider2,'Value')),[1:512], 'Color', [1 0 0])
axis off;
f =get(handles.axes1,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})
axes(handles.axes3)
imagesc(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:)));

    b=rot90(rot90(squeeze(handles.nifti(:,:,round(get(handles.slider3,'Value'))))));
    a= countour(b);
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    hold on
    b=imshow(blue);
    hold off
    set(b,'AlphaData',a/2);

set(gca,'DataAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
line([1:512],round(get(handles.slider2,'Value')), 'Color', [1 0 0])
axis off;
f =get(handles.axes3,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})


% --- Executes on button press in pXClockwize.
function pXClockwize_Callback(hObject, eventdata, handles)
% hObject    handle to pXClockwize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pXCounterClockwize.
function pXCounterClockwize_Callback(hObject, eventdata, handles)
% hObject    handle to pXCounterClockwize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pXTranslationPlus.
function pXTranslationPlus_Callback(hObject, eventdata, handles)
% hObject    handle to pXTranslationPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pXTranslationMinus.
function pXTranslationMinus_Callback(hObject, eventdata, handles)
% hObject    handle to pXTranslationMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pYTranslationPlus.
function pYTranslationPlus_Callback(hObject, eventdata, handles)
% hObject    handle to pYTranslationPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pYTranslationMinus.
function pYTranslationMinus_Callback(hObject, eventdata, handles)
% hObject    handle to pYTranslationMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pZTranslationMinus.
function pZTranslationMinus_Callback(hObject, eventdata, handles)
% hObject    handle to pZTranslationMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pZTranslationPlus.
function pZTranslationPlus_Callback(hObject, eventdata, handles)
% hObject    handle to pZTranslationPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pYClockwize.
function pYClockwize_Callback(hObject, eventdata, handles)
% hObject    handle to pYClockwize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pYCounterClockwize.
function pYCounterClockwize_Callback(hObject, eventdata, handles)
% hObject    handle to pYCounterClockwize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pZCounterClockwize.
function pZCounterClockwize_Callback(hObject, eventdata, handles)
% hObject    handle to pZCounterClockwize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pZClockwize.
function pZClockwize_Callback(hObject, eventdata, handles)
% hObject    handle to pZClockwize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
load('/home/vibratome/Dropbox/OCT+Vibratome/OCT_vibratome/NIFTI/interface/brain_structure.mat')
value=get(hObject,'Value');
string=cellstr(get(hObject,'String'));
a=char(string(value));
handles.bs=getfield(BS,char(a));

% Update display
    axes(handles.axes1)
    k=rot90(squeeze(handles.volume(:,:,512-round(get(handles.slider1,'Value')))));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    
    axis off;
    f =get(handles.axes1,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,2})
    axes(handles.axes2)
    k=rot90(squeeze(handles.volume(:,512-round(get(handles.slider2,'Value')),:)));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    
    axis off;
    f =get(handles.axes2,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,1})
    axes(handles.axes3)
    k=fliplr(rot90(squeeze(handles.volume(round(get(handles.slider3,'Value')),:,:))));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    axis off;
    f =get(handles.axes3,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
    
    % Update handles structure
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

