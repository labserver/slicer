function varargout = AtlasData_view(varargin)
% AtlasData_view MATLAB code for AtlasData_view.fig
%      AtlasData_view, by itself, creates a new AtlasData_view or raises the existing
%      singleton*.
%
%      H = AtlasData_view returns the handle to a new AtlasData_view or the handle to
%      the existing singleton*.
%
%      AtlasData_view('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AtlasData_view.M with the given input arguments.
%
%      AtlasData_view('Property','Value',...) creates a new AtlasData_view or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AtlasData_view_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AtlasData_view_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AtlasData_view

% Last Modified by GUIDE v2.5 24-Jan-2014 11:28:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AtlasData_view_OpeningFcn, ...
                   'gui_OutputFcn',  @AtlasData_view_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AtlasData_view is made visible.
function AtlasData_view_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AtlasData_view (see VARARGIN)

% Choose default command line output for AtlasData_view
handles.output = hObject;

set(handles.cWarpedAtlas,'Value',1)
set(handles.cWarpedData,'Value',0)
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AtlasData_view wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = AtlasData_view_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pLoad.
function pLoad_Callback(hObject, eventdata, handles)
% hObject    handle to pLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.folder = uigetdir;
handles.zoom = 0;
if get(handles.cWarpedData,'Value')
    load([handles.folder '/warpedData.mat']);
    load([handles.folder '/nifti_vol.mat']);
    handles.nifti = nifti_vol;
    handles.atlas = nifti_vol;
    handles.data = warpedImage;
elseif get(handles.cWarpedAtlas,'Value')
    load([handles.folder '/warpedAtlas.mat']);
    load([handles.folder '/iso_volume.mat']);
    handles.nifti = warpedImage;
    handles.atlas = warpedImage;
    handles.data = iso_volume;
end
handles.atlas=countour(handles.atlas);
handles.bs = 0;
% Update display
axes(handles.aMacroCoronal)

imagesc(squeeze(handles.data(:,:,end/2)));colormap gray; axis image
line=zeros(size(handles.data(:,:,1)));line(end/2:end/2+1,:)=1;line(:,end/2:end/2+1)=1;
    a=squeeze(handles.atlas(:,:,end/2));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroCoronal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})

axes(handles.aMacroSagittal)
imagesc(squeeze(handles.data(:,end/2,:))); colormap 'gray';axis image
line=zeros(size(squeeze(handles.data(:,1,:))));line(end/2:end/2+1,:)=1;line(:,end/2:end/2+1)=1;
    a=squeeze(handles.atlas(:,end/2,:));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroSagittal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})

axes(handles.aMacroTransversal)
imagesc(flipud(rot90(squeeze(handles.data(end/2,:,:))))); colormap 'gray';axis image
line=flipud(rot90(zeros(size(squeeze(handles.data(1,:,:))))));line(end/2:end/2+1,:)=1;line(:,end/2:end/2+1)=1;
    a=flipud(rot90(squeeze(handles.atlas(end/2,:,:))));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroTransversal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})

%Update Slider
%sMacroCoronal
set(handles.sMacroCoronal,'Max',size(handles.data,3))
set(handles.sMacroCoronal,'Min',1)
set(handles.sMacroCoronal, 'Value', round(size(handles.data,3)/2));
set(handles.sMacroCoronal, 'SliderStep', [1/(size(handles.data,3)-1) 1/(size(handles.data,3)-1)]);
set(handles.tMacroCoronal,'String', num2str(get(handles.sMacroCoronal,'Value')));
%sMacroSagittal
set(handles.sMacroSagittal,'Max',size(handles.data,2))
set(handles.sMacroSagittal,'Min',1)
set(handles.sMacroSagittal, 'Value', round(size(handles.data,2)/2));
set(handles.sMacroSagittal, 'SliderStep', [1/(size(handles.data,2)-1) 1/(size(handles.data,2)-1)]);
set(handles.tMacroSagittal,'String', num2str(get(handles.sMacroSagittal,'Value')));
%sTransversal
set(handles.sMacroTransversal,'Max',size(handles.data,1))
set(handles.sMacroTransversal,'Min',1)
set(handles.sMacroTransversal, 'Value', round(size(handles.data,1)/2));
set(handles.sMacroTransversal, 'SliderStep', [1/(size(handles.data,1)-1) 1/(size(handles.data,1)-1)]);
set(handles.tMacroTransversal,'String', num2str(get(handles.sMacroTransversal,'Value')));


% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in cWarpedAtlas.
function cWarpedAtlas_Callback(hObject, eventdata, handles)
% hObject    handle to cWarpedAtlas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cWarpedAtlas
set(handles.cWarpedData,'Value',0)

% --- Executes on button press in cWarpedData.
function cWarpedData_Callback(hObject, eventdata, handles)
% hObject    handle to cWarpedData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cWarpedData
set(handles.cWarpedAtlas,'Value',0)

% --- Executes on slider movement.
function sMicroCoronal_Callback(hObject, eventdata, handles)
% hObject    handle to sMicroCoronal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sMicroCoronal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sMicroCoronal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sMicroSagittal_Callback(hObject, eventdata, handles)
% hObject    handle to sMicroSagittal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sMicroSagittal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sMicroSagittal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sMicroTransversal_Callback(hObject, eventdata, handles)
% hObject    handle to sMicroTransversal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sMicroTransversal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sMicroTransversal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sMacroCoronal_Callback(hObject, eventdata, handles)
% hObject    handle to sMacroCoronal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tMacroCoronal,'String', num2str(round(get(handles.sMacroCoronal,'Value'))));
axes(handles.aMacroCoronal)
imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));axis image
    a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
    hold on
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    b=imagesc(blue);
    hold off
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroCoronal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})

axes(handles.aMacroSagittal)
imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));axis image
line=zeros(size(squeeze(handles.data(:,1,:))));line(:,round(get(handles.sMacroCoronal,'Value')):1+round(get(handles.sMacroCoronal,'Value')))=1;
    a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroSagittal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})

axes(handles.aMacroTransversal)
imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));axis image
line=flipud(rot90(zeros(size(squeeze(handles.data(1,:,:))))));line(round(get(handles.sMacroCoronal,'Value')):1+round(get(handles.sMacroCoronal,'Value')),:)=1;
    a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
    axis off;
f =get(handles.aMacroTransversal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})

% --- Executes during object creation, after setting all properties.
function sMacroCoronal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sMacroCoronal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sMacroSagittal_Callback(hObject, eventdata, handles)
% hObject    handle to sMacroSagittal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tMacroSagittal,'String', num2str(round(get(handles.sMacroSagittal,'Value'))));
axes(handles.aMacroCoronal)
imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));axis image
line=zeros(size(squeeze(handles.data(:,:,1))));line(:,round(get(handles.sMacroSagittal,'Value')):1+round(get(handles.sMacroSagittal,'Value')))=1;
    a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroCoronal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})

axes(handles.aMacroSagittal)
imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));axis image
    a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
    hold on
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    b=imagesc(blue);
    hold off
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroSagittal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})

axes(handles.aMacroTransversal)
imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));axis image
line=flipud(rot90(zeros(size(squeeze(handles.data(1,:,:))))));line(:,round(get(handles.sMacroSagittal,'Value')):1+round(get(handles.sMacroSagittal,'Value')))=1;
    a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
    axis off;
f =get(handles.aMacroTransversal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})

% --- Executes during object creation, after setting all properties.
function sMacroSagittal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sMacroSagittal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sMacroTransversal_Callback(hObject, eventdata, handles)
% hObject    handle to sMacroTransversal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tMacroTransversal,'String', num2str(round(get(handles.sMacroTransversal,'Value'))));
axes(handles.aMacroCoronal)
imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));axis image
line=zeros(size(squeeze(handles.data(:,:,1))));line(round(get(handles.sMacroTransversal,'Value')):1+round(get(handles.sMacroTransversal,'Value')),:)=1;
    a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroCoronal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})

axes(handles.aMacroSagittal)
imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));axis image
line=zeros(size(squeeze(handles.data(:,1,:))));line(round(get(handles.sMacroTransversal,'Value')):1+round(get(handles.sMacroTransversal,'Value')),:)=1;
    a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
    hold on
    red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    r=imagesc(red);
    b=imagesc(blue);
    hold off
    set(r,'AlphaData',line)
    set(b,'AlphaData',a/2)
axis off;
f =get(handles.aMacroSagittal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})

axes(handles.aMacroTransversal)
imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));axis image
    a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
    hold on
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    b=imagesc(blue);
    hold off
    set(b,'AlphaData',a/2)
    axis off;
f =get(handles.aMacroTransversal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})

% --- Executes during object creation, after setting all properties.
function sMacroTransversal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sMacroTransversal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
load('/home/vibratome/Dropbox/OCT+Vibratome/OCT_vibratome/NIFTI/interface/brain_structure.mat')
value=get(hObject,'Value');
string=cellstr(get(hObject,'String'));
a=char(string(value));
handles.bs=getfield(BS,char(a));

if get(handles.cWarpedData,'Value')
    %load([handles.folder '/nifti_vol.mat']);
    %handles.atlas = nifti_vol;
    handles.atlas = handles.nifti;
elseif get(handles.cWarpedAtlas,'Value')
    %load([handles.folder '/warpedAtlas.mat']);
    %handles.atlas = warpedImage;
    handles.atlas = handles.nifti;
end
if handles.bs==0
    handles.atlas=countour(handles.atlas);
elseif handles.bs~=0
    handles.atlas=structureHighlight(handles.atlas,handles.bs);
end


% Update display
    axes(handles.aMacroCoronal)
    imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));axis image
    a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
    hold on
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    b=imagesc(blue);
    hold off
    set(b,'AlphaData',a/2)
    axis off;
    f =get(handles.aMacroCoronal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,1})
    
    axes(handles.aMacroSagittal)
    imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));colormap gray;axis image
    a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    hold on
    r=imshow(blue);
    hold off
    set(r,'AlphaData',a/2);
    axis off;
    f =get(handles.aMacroSagittal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,2})
    
    axes(handles.aMacroTransversal)
    imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));colormap gray;axis image
    a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
    blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
    hold on
    r=imshow(blue);
    hold off
    set(r,'AlphaData',a/2);
    axis off;
    f =get(handles.aMacroTransversal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
    
    % Update handles structure
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in tZoom2.
function tZoom2_Callback(hObject, eventdata, handles)
% hObject    handle to tZoom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tZoom2
set(handles.tZoom20,'Value',0)
set(handles.tZoom5,'Value',0)
set(handles.tZoom10,'Value',0)

% --- Executes on button press in tZoom5.
function tZoom5_Callback(hObject, eventdata, handles)
% hObject    handle to tZoom5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tZoom5
set(handles.tZoom2,'Value',0)
set(handles.tZoom20,'Value',0)
set(handles.tZoom10,'Value',0)

% --- Executes on button press in tZoom10.
function tZoom10_Callback(hObject, eventdata, handles)
% hObject    handle to tZoom10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tZoom10
set(handles.tZoom2,'Value',0)
set(handles.tZoom5,'Value',0)
set(handles.tZoom20,'Value',0)

% --- Executes on button press in tZoom20.
function tZoom20_Callback(hObject, eventdata, handles)
% hObject    handle to tZoom20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tZoom20
set(handles.tZoom2,'Value',0)
set(handles.tZoom5,'Value',0)
set(handles.tZoom10,'Value',0)

function getpoints(src,evnt,handles,which_axe)

load([handles.folder '/deltaM.mat'])
load([handles.folder '/MemMapFileInfo.mat']);
handles.infox=info.x+300;
handles.infoy=info.y+300;
handles.infoz=info.z-sum(round(delta(3,:)))-size(delta,2);%+1+1;
f=memmapfile([handles.folder '/final.mat'], 'Format', {'uint16' [handles.infox handles.infoy handles.infoz] 'x'}, 'Writable', true);
if get(handles.tZoom2,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.aMacroCoronal,'CurrentPoint');
            axes(handles.aMacroCoronal)
                imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));colormap gray;axis image
                a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/4),1),round(1*size(a,2)/2)),min(max(round(cp(3)-size(a,1)/4),1),round(1*size(a,1)/2)), round(size(a,2)/2), round(size(a,1)/2)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroCoronal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/4)*handles.infox/size(a,1)),1),round(1*handles.infox/2)):min(max(round((cp(3)+size(a,1)/4)*handles.infox/size(a,1)),round(handles.infox/2+1)),round(handles.infox)),...
                min(max(round((cp(1)-size(a,2)/4)*handles.infoy/size(a,2)),1),round(1*handles.infoy/2)):min(max(round((cp(1)+size(a,2)/4)*handles.infoy/size(a,2)),round(handles.infoy/2+1)),round(handles.infoy)),...
                round(get(handles.sMacroCoronal,'Value')*handles.infoz/size(handles.data,3)))));colormap gray;axis image
            axis off
            f =get(handles.aMacroCoronal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
        case 2
            cp = get(handles.aMacroSagittal,'CurrentPoint');
            axes(handles.aMacroSagittal)
            imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));colormap gray;axis image
                a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/4),1),round(1*size(a,2)/2)),min(max(round(cp(3)-size(a,1)/4),1),round(1*size(a,1)/2)), round(size(a,2)/2), round(size(a,1)/2)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroSagittal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/4)*handles.infox/size(a,1)),1),round(1*handles.infox/2)):min(max(round((cp(3)+size(a,1)/4)*handles.infox/size(a,1)),round(handles.infox/2+1)),round(handles.infox)),...
                round(get(handles.sMacroSagittal,'Value')*handles.infoy/size(handles.data,2)),...
                min(max(round((cp(1)-size(a,2)/4)*handles.infoz/size(a,2)),1),round(1*handles.infoz/2)):min(max(round((cp(1)+size(a,2)/4)*handles.infoz/size(a,2)),round(handles.infoz/2+1)),round(handles.infoz)))));colormap gray;%axis image
                set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.aMacroSagittal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})

        case 3
            cp = get(handles.aMacroTransversal,'CurrentPoint');
            axes(handles.aMacroTransversal)
            imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));colormap gray;axis image
                a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/4),1),round(1*size(a,2)/2)),min(max(round(cp(3)-size(a,1)/4),1),round(1*size(a,1)/2)), round(size(a,2)/2), round(size(a,1)/2)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
            axis off;
            axes(handles.aMicroTransversal)
            imagesc(flipud(rot90(squeeze(f.Data(1).x(round(get(handles.sMacroTransversal,'Value')*handles.infox/size(handles.data,1)),...
                min(max(round((cp(1)-size(a,2)/4)*handles.infoy/size(a,2)),1),round(1*handles.infoy/2)):min(max(round((cp(1)+size(a,2)/4)*handles.infoy/size(a,2)),round(handles.infoy/2+1)),round(handles.infoy)),...
                min(max(round((cp(3)-size(a,1)/4)*handles.infoz/size(a,1)),1),round(1*handles.infoz/2)):min(max(round((cp(3)+size(a,1)/4)*handles.infoz/size(a,1)),round(handles.infoz/2+1)),round(handles.infoz)))))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            axis off
            f =get(handles.aMacroTransversal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})
            
    end
    
elseif get(handles.tZoom5,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.aMacroCoronal,'CurrentPoint');
            axes(handles.aMacroCoronal)
            imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));colormap gray;axis image
                a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/10),1),round(4*size(a,2)/5)),min(max(round(cp(3)-size(a,1)/10),1),round(4*size(a,1)/5)), round(size(a,2)/5), round(size(a,1)/5)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroCoronal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/10)*handles.infox/size(a,1)),1),round(4*handles.infox/5)):min(max(round((cp(3)+size(a,1)/10)*handles.infox/size(a,1)),round(handles.infox/5+1)),round(handles.infox)),...
                min(max(round((cp(1)-size(a,2)/10)*handles.infoy/size(a,2)),1),round(4*handles.infoy/5)):min(max(round((cp(1)+size(a,2)/10)*handles.infoy/size(a,2)),round(handles.infoy/5+1)),round(handles.infoy)),...
                round(get(handles.sMacroCoronal,'Value')*handles.infoz/size(handles.data,3)))));colormap gray;axis image
            axis off
            f =get(handles.aMacroCoronal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            
            
        case 2
            cp = get(handles.aMacroSagittal,'CurrentPoint');
            axes(handles.aMacroSagittal)
            imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));colormap gray;axis image
                a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/10),1),round(4*size(a,2)/5)),min(max(round(cp(3)-size(a,1)/10),1),round(4*size(a,1)/5)), round(size(a,2)/5), round(size(a,1)/5)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
             axes(handles.aMicroSagittal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/10)*handles.infox/size(a,1)),1),round(4*handles.infox/5)):min(max(round((cp(3)+size(a,1)/10)*handles.infox/size(a,1)),round(handles.infox/5+1)),round(handles.infox)),...
                round(get(handles.sMacroSagittal,'Value')*handles.infoy/size(handles.data,2)),...
                min(max(round((cp(1)-size(a,2)/10)*handles.infoz/size(a,2)),1),round(4*handles.infoz/5)):min(max(round((cp(1)+size(a,2)/10)*handles.infoz/size(a,2)),round(handles.infoz/5+1)),round(handles.infoz)))));colormap gray;%axis image
                set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.aMacroSagittal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})

        case 3
            cp = get(handles.aMacroTransversal,'CurrentPoint');
            axes(handles.aMacroTransversal)
            imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));colormap gray;axis image
                a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/10),1),round(4*size(a,2)/5)),min(max(round(cp(3)-size(a,1)/10),1),round(4*size(a,1)/5)), round(size(a,2)/5), round(size(a,1)/5)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroTransversal)
            imagesc(flipud(rot90(squeeze(f.Data(1).x(round(get(handles.sMacroTransversal,'Value')*handles.infox/size(handles.data,1)),...
                min(max(round((cp(1)-size(a,2)/10)*handles.infoy/size(a,2)),1),round(4*handles.infoy/5)):min(max(round((cp(1)+size(a,2)/10)*handles.infoy/size(a,2)),round(handles.infoy/5+1)),round(handles.infoy)),...
                min(max(round((cp(3)-size(a,1)/10)*handles.infoz/size(a,1)),1),round(4*handles.infoz/5)):min(max(round((cp(3)+size(a,1)/10)*handles.infoz/size(a,1)),round(handles.infoz/5+1)),round(handles.infoz)))))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            axis off
            f =get(handles.aMacroTransversal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})

    end
elseif get(handles.tZoom10,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.aMacroCoronal,'CurrentPoint');
            axes(handles.aMacroCoronal)
            imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));colormap gray;axis image
                a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/20),1),round(9*size(a,2)/10)),min(max(round(cp(3)-size(a,1)/20),1),round(9*size(a,1)/10)), round(size(a,2)/10), round(size(a,1)/10)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroCoronal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/20)*handles.infox/size(a,1)),1),round(9*handles.infox/10)):min(max(round((cp(3)+size(a,1)/20)*handles.infox/size(a,1)),round(handles.infox/10+1)),round(handles.infox)),...
                min(max(round((cp(1)-size(a,2)/20)*handles.infoy/size(a,2)),1),round(9*handles.infoy/10)):min(max(round((cp(1)+size(a,2)/20)*handles.infoy/size(a,2)),round(handles.infoy/10+1)),round(handles.infoy)),...
                round(get(handles.sMacroCoronal,'Value')*handles.infoz/size(handles.data,3)))));colormap gray;axis image
            axis off
            f =get(handles.aMacroCoronal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            
            
        case 2
            cp = get(handles.aMacroSagittal,'CurrentPoint');
            axes(handles.aMacroSagittal)
            imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));colormap gray;axis image
                a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/20),1),round(9*size(a,2)/10)),min(max(round(cp(3)-size(a,1)/20),1),round(9*size(a,1)/10)), round(size(a,2)/10), round(size(a,1)/10)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroSagittal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/20)*handles.infox/size(a,1)),1),round(9*handles.infox/10)):min(max(round((cp(3)+size(a,1)/20)*handles.infox/size(a,1)),round(handles.infox/10+1)),round(handles.infox)),...
                round(get(handles.sMacroSagittal,'Value')*handles.infoy/size(handles.data,2)),...
                min(max(round((cp(1)-size(a,2)/20)*handles.infoz/size(a,2)),1),round(9*handles.infoz/10)):min(max(round((cp(1)+size(a,2)/20)*handles.infoz/size(a,2)),round(handles.infoz/10+1)),round(handles.infoz)))));colormap gray;%axis image
                set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.aMacroSagittal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})

        case 3
            cp = get(handles.aMacroTransversal,'CurrentPoint');
            axes(handles.aMacroTransversal)
            imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));colormap gray;axis image
                a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/20),1),round(9*size(a,2)/10)),min(max(round(cp(3)-size(a,1)/20),1),round(9*size(a,1)/10)), round(size(a,2)/10), round(size(a,1)/10)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroTransversal)
            imagesc(flipud(rot90(squeeze(f.Data(1).x(round(get(handles.sMacroTransversal,'Value')*handles.infox/size(handles.data,1)),...
                min(max(round((cp(1)-size(a,2)/20)*handles.infoy/size(a,2)),1),round(9*handles.infoy/10)):min(max(round((cp(1)+size(a,2)/20)*handles.infoy/size(a,2)),round(handles.infoy/10+1)),round(handles.infoy)),...
                min(max(round((cp(3)-size(a,1)/20)*handles.infoz/size(a,1)),1),round(9*handles.infoz/10)):min(max(round((cp(3)+size(a,1)/20)*handles.infoz/size(a,1)),round(handles.infoz/10+1)),round(handles.infoz)))))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            axis off
            f =get(handles.aMacroTransversal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})

    end
elseif get(handles.tZoom20,'Value')
    switch(which_axe)
        case 1
            cp = get(handles.aMacroCoronal,'CurrentPoint');
            axes(handles.aMacroCoronal)
            imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));colormap gray;axis image
                a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/40),1),round(19*size(a,2)/20)),min(max(round(cp(3)-size(a,1)/40),1),round(19*size(a,1)/20)), round(size(a,2)/20), round(size(a,1)/20)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroCoronal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/40)*handles.infox/size(a,1)),1),round(19*handles.infox/20)):min(max(round((cp(3)+size(a,1)/40)*handles.infox/size(a,1)),round(handles.infox/20+1)),round(handles.infox)),...
                min(max(round((cp(1)-size(a,2)/40)*handles.infoy/size(a,2)),1),round(19*handles.infoy/20)):min(max(round((cp(1)+size(a,2)/40)*handles.infoy/size(a,2)),round(handles.infoy/20+1)),round(handles.infoy)),...
                round(get(handles.sMacroCoronal,'Value')*handles.infoz/size(handles.data,3)))));colormap gray;axis image
            axis off
            f =get(handles.aMacroCoronal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,1})
            
            
        case 2
            cp = get(handles.aMacroSagittal,'CurrentPoint');
            axes(handles.aMacroSagittal)
            imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));colormap gray;axis image
                a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/40),1),round(19*size(a,2)/20)),min(max(round(cp(3)-size(a,1)/40),1),round(19*size(a,1)/20)), round(size(a,2)/20), round(size(a,1)/20)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroSagittal)
                imagesc(squeeze(f.Data(1).x(min(max(round((cp(3)-size(a,1)/40)*handles.infox/size(a,1)),1),round(19*handles.infox/20)):min(max(round((cp(3)+size(a,1)/40)*handles.infox/size(a,1)),round(handles.infox/20+1)),round(handles.infox)),...
                round(get(handles.sMacroSagittal,'Value')*handles.infoy/size(handles.data,2)),...
                min(max(round((cp(1)-size(a,2)/40)*handles.infoz/size(a,2)),1),round(19*handles.infoz/20)):min(max(round((cp(1)+size(a,2)/40)*handles.infoz/size(a,2)),round(handles.infoz/20+1)),round(handles.infoz)))));colormap gray;%axis image
                set(gca,'PlotBoxAspectRatio',[(handles.infoz*512/154)/handles.infoy handles.infox/handles.infoy 1])
            axis off
            f =get(handles.aMacroSagittal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,2})

        case 3
            cp = get(handles.aMacroTransversal,'CurrentPoint');
            axes(handles.aMacroTransversal)
            imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));colormap gray;axis image
                a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
                rectangle('Position',[min(max(round(cp(1)-size(a,2)/40),1),round(19*size(a,2)/20)),min(max(round(cp(3)-size(a,1)/40),1),round(19*size(a,1)/20)), round(size(a,2)/20), round(size(a,1)/20)], 'EdgeColor', [1 0 0])
                hold on
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                b=imagesc(blue);
                hold off
                set(b,'AlphaData',a/2)
                axis off;
            axes(handles.aMicroTransversal)
            imagesc(flipud(rot90(squeeze(f.Data(1).x(round(get(handles.sMacroTransversal,'Value')*handles.infox/size(handles.data,1)),...
                min(max(round((cp(1)-size(a,2)/40)*handles.infoy/size(a,2)),1),round(19*handles.infoy/20)):min(max(round((cp(1)+size(a,2)/40)*handles.infoy/size(a,2)),round(handles.infoy/20+1)),round(handles.infoy)),...
                min(max(round((cp(3)-size(a,1)/40)*handles.infoz/size(a,1)),1),round(19*handles.infoz/20)):min(max(round((cp(3)+size(a,1)/40)*handles.infoz/size(a,1)),round(handles.infoz/20+1)),round(handles.infoz)))))));
            set(gca,'PlotBoxAspectRatio',[handles.infoy/handles.infoy (handles.infoz*512/154)/handles.infoy 1])
            axis off
            f =get(handles.aMacroTransversal,'Children');
            set(f,'ButtonDownFcn',{@getpoints,handles,3})

    end
else
    switch(which_axe)
        
        case 1
            cp = get(handles.aMacroCoronal,'CurrentPoint');
            cp(1)=round(cp(1));
            cp(3)=round(cp(3));
            axes(handles.aMacroCoronal)
                imagesc(squeeze(handles.data(:,:,round(get(handles.sMacroCoronal,'Value')))));colormap gray;axis image
                line=zeros(size(squeeze(handles.data(:,:,1))));line(cp(3):1+cp(3),:)=1;line(:,cp(1):1+cp(1))=1;
                a=squeeze(handles.atlas(:,:,round(get(handles.sMacroCoronal,'Value'))));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroCoronal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,1})
            axes(handles.aMacroSagittal)
                imagesc(squeeze(handles.data(:,round(cp(1)),:)));colormap gray;axis image
                line=zeros(size(squeeze(handles.data(:,1,:))));line(cp(3):1+cp(3),:)=1;line(:,round(get(handles.sMacroCoronal,'Value')):1+round(get(handles.sMacroCoronal,'Value')))=1;
                a=squeeze(handles.atlas(:,round(cp(1)),:));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroSagittal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,2})
            axes(handles.aMacroTransversal)
                imagesc(flipud(rot90(squeeze(handles.data(round(cp(3)),:,:)))));colormap gray;axis image
                line=flipud(rot90(zeros(size(squeeze(handles.data(1,:,:))))));line(:,cp(1):1+cp(1))=1;line(round(get(handles.sMacroCoronal,'Value')):1+round(get(handles.sMacroCoronal,'Value')),:)=1;
                a=flipud(rot90(squeeze(handles.atlas(round(cp(3)),:,:))));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroTransversal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,3})
            set(handles.sMacroSagittal, 'Value', round(cp(1)));
            set(handles.tMacroSagittal,'String', num2str(round(cp(1))));
            set(handles.sMacroTransversal, 'Value', round(cp(3)));
            set(handles.tMacroTransversal,'String', num2str(round(cp(3))));

        case 2
            cp = get(handles.aMacroSagittal,'CurrentPoint');
            cp(1)=round(cp(1));
            cp(3)=round(cp(3));
            axes(handles.aMacroCoronal)
                imagesc(squeeze(handles.data(:,:,round(cp(1)))));colormap gray;axis image
                line=zeros(size(squeeze(handles.data(:,:,1))));line(cp(3):1+cp(3),:)=1;line(:,round(get(handles.sMacroSagittal,'Value')):1+round(get(handles.sMacroSagittal,'Value')))=1;
                a=squeeze(handles.atlas(:,:,round(cp(1))));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroCoronal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,1})
            axes(handles.aMacroSagittal)
                imagesc(squeeze(handles.data(:,round(get(handles.sMacroSagittal,'Value')),:)));colormap gray;axis image
                line=zeros(size(squeeze(handles.data(:,1,:))));line(:,cp(1):1+cp(1))=1;line(cp(3):1+cp(3),:)=1;
                a=squeeze(handles.atlas(:,round(get(handles.sMacroSagittal,'Value')),:));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroSagittal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,2})
            axes(handles.aMacroTransversal)
                imagesc(flipud(rot90(squeeze(handles.data(round(cp(3)),:,:)))));colormap gray;axis image
                line=flipud(rot90(zeros(size(squeeze(handles.data(1,:,:))))));line(cp(1):1+cp(1),:)=1;line(:,round(get(handles.sMacroSagittal,'Value')):1+round(get(handles.sMacroSagittal,'Value')))=1;
                a=flipud(rot90(squeeze(handles.atlas(round(cp(3)),:,:))));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroTransversal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,3})
            set(handles.sMacroCoronal, 'Value', round(cp(1)));
            set(handles.tMacroCoronal,'String', num2str(round(cp(1))));
            set(handles.sMacroTransversal, 'Value', round(cp(3)));
            set(handles.tMacroTransversal,'String', num2str(round(cp(3))));

        case 3
            cp = get(handles.aMacroTransversal,'CurrentPoint');
            cp(1)=round(cp(1));
            cp(3)=round(cp(3));
            axes(handles.aMacroCoronal)
                imagesc(squeeze(handles.data(:,:,round(cp(3)))));colormap gray;axis image
                line=zeros(size(squeeze(handles.data(:,:,1))));line(:,cp(1):1+cp(1))=1;line(round(get(handles.sMacroTransversal,'Value')):1+round(get(handles.sMacroTransversal,'Value')),:)=1;
                a=squeeze(handles.atlas(:,:,round(cp(3))));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroCoronal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,1})
            axes(handles.aMacroSagittal)
                imagesc(squeeze(handles.data(:,round(cp(1)),:)));colormap gray;axis image
                line=zeros(size(squeeze(handles.data(:,1,:))));line(:,cp(3):1+cp(3))=1;line(round(get(handles.sMacroTransversal,'Value')):1+round(get(handles.sMacroTransversal,'Value')),:)=1;
                a=squeeze(handles.atlas(:,round(cp(1)),:));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroSagittal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,2})
            axes(handles.aMacroTransversal)
                imagesc(flipud(rot90(squeeze(handles.data(round(get(handles.sMacroTransversal,'Value')),:,:)))));colormap gray;axis image
                line=flipud(rot90(zeros(size(squeeze(handles.data(1,:,:))))));line(:,cp(1):1+cp(1))=1;line(cp(3):1+cp(3),:)=1;
                a=flipud(rot90(squeeze(handles.atlas(round(get(handles.sMacroTransversal,'Value')),:,:))));
                hold on
                red=cat(3,ones(size(line)),zeros(size(line)),zeros(size(line)));
                blue=cat(3,zeros(size(a)),zeros(size(a)),ones(size(a)));
                r=imagesc(red);
                b=imagesc(blue);
                hold off
                set(r,'AlphaData',line)
                set(b,'AlphaData',a/2)
                axis off;
                f =get(handles.aMacroTransversal,'Children');
                set(f,'ButtonDownFcn',{@getpoints,handles,3})
            set(handles.sMacroCoronal, 'Value', round(cp(3)));
            set(handles.tMacroCoronal,'String', num2str(round(cp(3))));
            set(handles.sMacroSagittal, 'Value', round(cp(3)));
            set(handles.tMacroSagittal,'String', num2str(round(cp(3))));

    end
end

