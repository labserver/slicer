%I have just tried to fix up the post so the code is useable first with an example on a single translated 
%imaage and then on a more general two image input case. Here is working code for an example that uses a 
%single image and cuts out a 256x256 pixel portion of the image from one location and then cuts out the 
%same size portion from a location that has been translated by 11 pixels in the x direction and 7 
%pixels in the y direction. This shows that the basic technique does work when you have the code formatting correct.

image1 = rgb2gray(imread('frame429.jpg')); % read in image and convert from RGB ro grayscale
rect1 = [1 1 255 255]; % define first cutout region [xmin ymin width height]
rect2 = [12 8 255 255]; % define second translated cutout region [xmin ymin width height]
sub_image1 = imcrop(image1,rect1); % 1st cut out portion of original image 
sub_image2 = imcrop(image1,rect2); % 2nd cut out portion of original image 
filter1=blackman(length(sub_image1),'symmetric');
filtimg1=(filter1*filter1');
Image1=double(sub_image1).*filtimg1;
Image2=double(sub_image2).*filtimg1
FFT1 = fft2(Image1);
FFT2 = conj(fft2(Image2));
FFTR = FFT1.*FFT2;
magFFTR = abs(FFTR);
FFTRN = (FFTR./magFFTR);
result = ifft2(double(FFTRN)); 
figure;
colormap('gray'); % choose colormap for plotting
imagesc(result);
figure;
colormap(jet);
mesh(result);

%The code can easily be changed to use two different images of the same dimensions:

image1 = rgb2gray(imread('frame429.jpg')); % read image 1 (convert to grayscale if RGB)
image2 = rgb2gray(imread('frame430.jpg')); % read image 2 must have same dimensions as image 1


image1 = squeeze(fft221(:,:,12)); 
image2 = squeeze(fft211(:,:,12));
filter1=blackman(size(image1,1),'symmetric'); % setup window in one direction - I chose Blackman window - could use Hamming or others
filter2=blackman(size(image1,2),'symmetric'); % setup window in second direction
filtimg1=(filter1*filter2'); % make 2D windowing function that is sized to match image
Image1=double(image1).*filtimg1; % apply windowing function to image
Image2=double(image2).*filtimg1;
FFT1 = fft2(Image1); % 2d FFT
FFT2 = conj(fft2(Image2)); 
FFTR = FFT1.*FFT2;
magFFTR = abs(FFTR);
FFTRN = (FFTR./magFFTR);
result = ifft2(double(FFTRN)); 
figure;
colormap('gray'); % choose colormap for plotting
imagesc(log(result));
figure;
colormap(jet);
mesh(result);