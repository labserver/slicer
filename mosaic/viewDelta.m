function viewDelta(directory, axe)
load([directory 'delta.mat']);

nbSlices=size(delta,4);

for i=1:nbSlices
    imagesc(squeeze(delta(axe,:,:,i)));title(num2str(i));colorbar;pause(0.3);end;


% 
% m1=sum(sum(delta(1,1,2:end,:)))/length(find(delta(1,1,2:end,:)));
% m2=sum(sum(delta(2,1,2:end,:)))/length(find(delta(2,1,2:end,:)));
% m3=sum(sum(delta(3,1,2:end,:)))/length(find(delta(3,1,2:end,:)));
% s1=std(std(delta(1,1,2:end,:)));
% s2=std(std(delta(2,1,2:end,:)));
% s3=std(std(delta(3,1,2:end,:)));
% m4=mean(mean(mean(delta(1,2:end,:,:))));
% m5=mean(mean(mean(delta(2,2:end,:,:))));
% m6=mean(mean(mean(delta(3,2:end,:,:))));
% s4=std(std(std(delta(1,2:end,:,:))));
% s5=std(std(std(delta(2,2:end,:,:))));
% s6=std(std(std(delta(3,2:end,:,:))));
% for z = 1:size(delta,4)
%     for y = 1:size(delta,3)
%         for x = 1: size(delta,2)
%             if (x==1)&&(y==1)&&(z~=1)&&(sum(delta(:,x,y,z),1) < 10)
%                 delta(1,1,y,z) = 30;
%                 delta(2,x,y,z) = 0;
%                 delta(3,x,y,z) = 0;
%                 
%             elseif (x==1)&&(y~=1)
%                 %                 if abs(delta(1,x,y,z)-m1)>5
%                 %                     delta(:,x,y,z)=0;
%                 %                 end
%                 %                 if abs(delta(2,x,y,z)-m2)>5
%                 %                     delta(:,x,y,z)=0;
%                 %                 end
%                 if abs(mean(delta(:,x,y,z))-mean(mean(mean(delta(:,1,2:end,:)))))>std(std(std(delta(:,1,2:end,:))))
%                     delta(:,x,y,z)=0;
%                 end
%                 if abs(delta(3,x,y,z)-m3)>2*abs(s3)
%                     delta(:,x,y,z)=0;
%                 end
%                 %                 delta(1,x,y,z) = a1;
%                 %                 delta(2,x,y,z) = a2;
%                 %                 delta(3,x,y,z) = a3;
%                 
%             elseif (x~=1)
%                 %                 if abs(delta(1,x,y,z)-m4)>2*abs(s4)
%                 %                     delta(1,x,y,z)=0;
%                 %                 end
%                 if abs(delta(2,x,y,z)-m5)>2*abs(s5)
%                     delta(:,x,y,z)=0;
%                 end
%                 if abs(mean(delta(:,x,y,z))-mean(mean(mean(mean(delta(:,2:end,:,:))))))>2*std(std(std(std(delta(:,2:end,:,:)))))
%                     delta(:,x,y,z)=0;
%                 end
%                 %                 if abs(delta(3,x,y,z)-m6)>2*abs(s6)
%                 %                     delta(3,x,y,z)=0;
%                 %                 end
%                 %                 delta(1,x,y,z) = b1;
%                 %                 delta(2,x,y,z) = b2;
%                 %                 delta(3,x,y,z) = b3;
%                 
%             end
%         end
%     end
% end
% 
% 
% 
% a1=sum(sum(sum(delta(1,1,2:end,:))))/length(find(delta(1,1,2:end,:)));
% a2=sum(sum(sum(delta(2,1,2:end,:))))/length(find(delta(2,1,2:end,:)));
% a3=sum(sum(sum(delta(3,1,2:end,:))))/length(find(delta(3,1,2:end,:)));
% 
% %(x~=1)&&(sum(delta(:,x,y,z),1) < 10)
% b1=sum(sum(sum(delta(1,2:end,:,:))))/length(find(delta(1,2:end,:,:)));
% b2=sum(sum(sum(delta(2,2:end,:,:))))/length(find(delta(2,2:end,:,:)));
% b3=sum(sum(sum(delta(3,2:end,:,:))))/length(find(delta(3,2:end,:,:)));
% 
% for z = 1:size(delta,4)
%     for y = 1:size(delta,3)
%         for x = 1: size(delta,2)
%             if (x==1)&&(y==1)&&(z~=1)&&(sum(delta(:,x,y,z),1) < 10)
%                 delta(1,1,y,z) = 30;
%                 delta(2,x,y,z) = 0;
%                 delta(3,x,y,z) = 0;
%                 
%             elseif (x==1)&&(y~=1)&& sum(delta(:,x,y,z))==0
%                 
%                 delta(1,x,y,z) = a1;
%                 delta(2,x,y,z) = a2;
%                 delta(3,x,y,z) = a3;
%                 
%             elseif (x~=1)&&sum(delta(:,x,y,z))==0
%                 
%                 delta(1,x,y,z) = b1;
%                 delta(2,x,y,z) = b2;
%                 delta(3,x,y,z) = b3;
%                 
%             end
%         end
%     end
% end
% 
% 
% 
% 
% for z = 1:size(delta,4)
%     for y = 1:size(delta,3)
%         for x = 1: size(delta,2)
%             if (x==1)&&(y==1)&&(z~=1)&&(sum(delta(:,x,y,z),1) < 10)
%                 delta(1,1,y,z) = 30;
%                 delta(2,x,y,z) = 0;
%                 delta(3,x,y,z) = 0;
%                 
%             elseif (x==1)&&(y~=1)&&abs(mean(delta(:,x,y,z))-mean(mean(mean(delta(:,1,2:end,:)))))>std(std(std(delta(:,1,2:end,:))))
%                 delta(:,x,y,z)=0;
%                 
%             elseif (x~=1)&&abs(mean(delta(:,x,y,z))-mean(mean(mean(mean(delta(:,2:end,:,:))))))>2*std(std(std(std(delta(:,2:end,:,:)))))
%                 delta(:,x,y,z)=0;
%                 
%                 
%             end
%         end
%     end
% end
% 
