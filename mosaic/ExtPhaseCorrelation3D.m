%Implementation of:
%   Extension of Phase Correlation to Subpixel Registration
%   (Hassan Foroosh, Josiane B. Zerubia, Marc Berthod)
%Implemented by:
%   Lulu Ardiansyah (halluvme@gmail.com)
%
%TODO:
%   - Find out whether this implementation is correct :)
%   - Combine the result, overlay the images based on the result
%
%                                               eL-ardiansyah
%                                               January, 2010
%                                                       CMIIW
%============================================================
function [deltaX , deltaY, deltaZ] = ExtPhaseCorrelation3D(img1, img2)
%Description:
%   Find the translation shift between two image
%
%Parameter:
%   img1 = image 1
%   img2 = image 2
%   image 1 and image 2 , must in the same size

%Phase correlation (Kuglin & Hines, 1975)
%============================================================

%    img1 = z01_x02_y01;
%    img2 = z01_x01_y01;

[M, N, P] = size(img1);

af = fftn(double(img1));
bf = fftn(double(img2));
cp = af .* conj(bf) ./ abs(af .* conj(bf));
icp = (ifftn(cp));
mmax = max(max(max(icp)));

%Find the main peak
[x,col] = find(icp == mmax);
[y,z] = ind2sub([N P],col);
%figure; imshow(icp,[],'notruesize');

%Extension to Subpixel Registration [Foroosh, Zerubia & Berthod, 2002]
%============================================================


%two side-peaks
xsp = x + 1;
xsn = x - 1;
ysp = y + 1;
ysn = y - 1;
zsp = z + 1;
zsn = z - 1;

%if the peaks outsize the image, then use xsn and/or ysn for the two
%side-peaks
if xsp > M
    xsp = M-1;
end
if ysp > N
    ysp = N-1;
end
if zsp > P
    zsp = P-1;
end

%Calculate the translational shift
deltaX1 = ((icp(xsp,y,z) * xsp + icp(x,y,z) * x) / (icp(xsp,y,z) + icp(x,y,z)))-1;
deltaY1 = ((icp(x,ysp,z) * ysp + icp(x,y,z) * y) / (icp(x,ysp,z) + icp(x,y,z)))-1;
deltaZ1 = ((icp(x,y,zsp) * zsp + icp(x,y,z) * z) / (icp(x,y,zsp) + icp(x,y,z)))-1;


%I don't know why but after few test i find out that the result of deltaX
%and delta Y is inverted.. :( ??

%Validate if translation shift is negative
if deltaX1 < (M/2)
     deltaZ = deltaX1;
else
    deltaZ = deltaX1 - M;
end

if deltaY1 < (N/2)
     deltaX = deltaY1;
else
    deltaX = deltaY1 - N;
end

if deltaZ1 < (P/2)
     deltaY = deltaZ1;
else
    deltaY = deltaZ1 - P;
end

if abs(deltaZ) < M/10
    deltaZ = deltaZ;
else
    deltaZ = deltaZ + M;
end

if abs(deltaX) < N/10
    deltaX = deltaX;
else
    deltaX = deltaX + N;
end

if abs(deltaY) < P/10
    deltaY = deltaY;
else
    deltaY = deltaY + P;
end