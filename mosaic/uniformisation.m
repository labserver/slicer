function uniformisation(directory, nslice, npixel, Xmin, Xmax, Ymin, Ymax)
% Une fois les slices individuelles formées, uniformisation donne la même
% taille à tous les images et les reconstruit dans un memmapfile.
% L'uniformisation de la taille est nécessaire pour la fonction
% shift_mosaic
for z = 1:nslice
    tic
    for p = 1:npixel
        s= load([directory 'slice' sprintf( '%03.0f', z) '_pixel' sprintf( '%03.0f', p) '.mat']);
        s=s.slice;
        s = squeeze(s(1,Xmin:Xmax,Ymin:Ymax));
       % s = rot90(s);
        s = filtrage(s, 1);
        u=s./max(max(s));
        u = uint16(u.*2^16);
        if (z==1)&(p==1)
            fid = fopen([directory 'MemMapFile.mat'], 'w');
            fwrite(fid,u, 'uint16');
        else
            fid = fopen([directory 'MemMapFile.mat'], 'a');
            fwrite(fid,u, 'uint16');
        end
        %save([directory 'Uniformisation/slice' sprintf( '%03.0f', z) '_pixel' sprintf( '%03.0f', p)  '.mat'],'s');
        clear s
        clear u
    end
    toc
end
info.x = Ymax-Ymin+1;
info.y = Xmax-Xmin+1;
info.z = nslice*npixel;
save([directory 'MemMapFileInfo.mat'], 'info');

%Cerveau souris
%430: 5300     X
% 700 : 4080   Y