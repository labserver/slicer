%% calcul des d�callages
function [delta] = shift(directory, frameX, frameY,frameZ)
% main function for measuring shifts between volumes
if exist([directory 'meanVolume.mat'])
    load([directory 'meanVolume.mat']);
else
    meanVolume(directory, frameX, frameY,frameZ)
    load([directory 'meanVolume.mat']);
end
tic
delta = zeros(3,frameX,frameY,frameZ);

for z = 1:frameZ
    for y = 1:frameY
        tic
        for x = 1: frameX-1
            if exist('volumePost');
                volumePre = volumePost;
                clear volumePost;
            else
                fid = fopen([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'rb');
                volumePre = fread(fid,inf, 'uint16');
                volumePre = reshape(volumePre,[200 512 512]);
                fclose(fid);
                %  VolumePre = load([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
                %                 for i = 1:size(VolumePre.FFTIMG,2)
                %                     for j = 1: size(VolumePre.FFTIMG,3)
                %                         a(i,j) = find(VolumePre.FFTIMG(:,i,j)==max(VolumePre.FFTIMG(:,i,j)));
                %                     end
                %                 end
                %                 meanA = round(mean(mean(a)));
                %                 clear a
                volumePre = volumePre./sumV;
                volumePre = volumePre(1:100,:,:);
            end
            % deuxieme volume
            fid = fopen([directory 'volume_' sprintf( 'x%02.0f', x+1) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'rb');
            volumePost = fread(fid,inf, 'uint16');
            volumePost = reshape(volumePost,[200 512 512]);
            fclose(fid);
            %VolumePost = load([directory 'volume_' sprintf( 'x%02.0f', x+1) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
            %             for i = 1:size(VolumePost.FFTIMG,2)
            %                 for j = 1: size(VolumePost.FFTIMG,3)
            %           a(i,j) = find(VolumePost.FFTIMG(:,i,j)==max(VolumePost.FFTIMG(:,i,j)));
            %                 end
            %             end
            %             meanA = round(mean(mean(a)));
            %             clear a
            volumePost = volumePost./sumV;
            volumePost = volumePost(1:100,:,:);
            
            %Calcul du decallage
            [deltaX , deltaY, deltaZ] = ExtPhaseCorrelation3D(volumePre, volumePost);
            delta(:,x+1,y,z) =  [deltaZ deltaX deltaY];
        end
        if y ~= frameY
            fid = fopen([directory 'volume_' sprintf( 'x%02.0f', round(frameX/2)) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'rb');
            volumePre = fread(fid,inf, 'uint16');
            volumePre = reshape(volumePre,[200 512 512]);
            fclose(fid);
            %VolumePre = load([directory 'volume_' sprintf( 'x%02.0f', round(frameX/2)) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
            %             for i = 1:size(VolumePre.FFTIMG,2)
            %                 for j = 1: size(VolumePre.FFTIMG,3)
            %                     a(i,j) = find(VolumePre.FFTIMG(:,i,j)==max(VolumePre.FFTIMG(:,i,j)));
            %                 end
            %             end
            %             meanA = round(mean(mean(a)));
            %             clear a
            volumePre = volumePre./sumV;
            volumePre = volumePre(1:100,:,:);
            fid = fopen([directory 'volume_' sprintf( 'x%02.0f', round(frameX/2)) '_' sprintf( 'y%02.0f', y+1) '_' sprintf( 'z%02.0f', z) '.mat'],'rb');
            
            volumePost = fread(fid,inf, 'uint16');
            volumePost = reshape(volumePost,[200 512 512]);
            fclose(fid);
            % VolumePost = load([directory 'volume_' sprintf( 'x%02.0f', round(frameX/2)) '_' sprintf( 'y%02.0f', y+1) '_' sprintf( 'z%02.0f', z) '.mat']);
            %             for i = 1:size(VolumePost.FFTIMG,2)
            %                 for j = 1: size(VolumePost.FFTIMG,3)
            %                     a(i,j) = find(VolumePost.FFTIMG(:,i,j)==max(VolumePost.FFTIMG(:,i,j)));
            %                 end
            %             end
            %             meanA = round(mean(mean(a)));
            %             clear a
            volumePost = volumePost./sumV;
            volumePost = volumePost(1:100,:,:);
            [deltaX , deltaY, deltaZ] = ExtPhaseCorrelation3D(volumePre, volumePost);
            delta(:,1,y+1,z) = [deltaZ deltaX deltaY];
        end
        toc
    end
    if z ~= frameZ
        fid = fopen([directory 'volume_' sprintf( 'x%02.0f', 1) '_' sprintf( 'y%02.0f', 1) '_' sprintf( 'z%02.0f', z) '.mat'],'rb');
        volumePre = fread(fid,inf, 'uint16');
        volumePre = reshape(volumePre,[200 512 512]);
        fclose(fid);
        %VolumePre = load([directory 'volume_' sprintf( 'x%02.0f', 1) '_' sprintf( 'y%02.0f', 1) '_' sprintf( 'z%02.0f', z) '.mat']);
        %         for i = 1:size(VolumePre.FFTIMG,2)
        %             for j = 1: size(VolumePre.FFTIMG,3)
        %                 a(i,j) = find(VolumePre.FFTIMG(:,i,j)==max(VolumePre.FFTIMG(:,i,j)));
        %             end
        %         end
        %         meanA = round(mean(mean(a)));
        %         clear a
        volumePre = volumePre./sumV;
        volumePre = volumePre(1:100,:,:);
        fid = fopen([directory 'volume_' sprintf( 'x%02.0f', 1) '_' sprintf( 'y%02.0f', 1) '_' sprintf( 'z%02.0f', z+1) '.mat'],'rb');
        volumePost = fread(fid,inf, 'uint16');
        volumePost = reshape(volumePost,[200 512 512]);
        fclose(fid);
        %VolumePost = load([directory 'volume_' sprintf( 'x%02.0f', 1) '_' sprintf( 'y%02.0f', 1) '_' sprintf( 'z%02.0f', z+1) '.mat']);
        %         for i = 1:size(VolumePost.FFTIMG,2)
        %             for j = 1: size(VolumePost.FFTIMG,3)
        %                 a(i,j) = find(VolumePost.FFTIMG(:,i,j)==max(VolumePost.FFTIMG(:,i,j)));
        %             end
        %         end
        %         meanA = round(mean(mean(a)));
        %         clear a
        volumePost = volumePost./sumV;
        volumePost = volumePost(1:100,:,:);
        [deltaX , deltaY, deltaZ] = ExtPhaseCorrelation3D(volumePre, volumePost);
        %delta(:,1,1,z+1) = [deltaZ deltaX deltaY];
        delta(:,1,1,z+1) = [0 0 0];
    end
end

delta = round(delta);
save([directory 'delta.mat'],'delta');

dY = delta(3,1,2:end,:);
dX = delta(2,2:end,:,:);
for z = 1:size(delta,4)
    for y = 1:size(delta,3)
        for x = 1: size(delta,2)
            if (x==1)&&(y~=1)&&abs(mean(dY(find(dY)))-delta(3,x,y,z))>20%(std(dY(find(dY)))/3)
                delta(:,x,y,z)=0;
            elseif (x~=1)&&abs(mean(dX(find(dX)))-delta(2,x,y,z))>20%(std(dX(find(dX)))/5)
                delta(:,x,y,z)=0;
            end
        end
    end
end

dY = mean(delta(:,1,2:end,:),1);
dX = mean(delta(:,2:end,:,:),1);
for z = 1:size(delta,4)
    for y = 1:size(delta,3)
        for x = 1: size(delta,2)
            if (x==1)&&(y~=1)&&abs(mean(delta(:,x,y,z))-mean(dY(find(dY))))>std(dY(find(dY)))
                delta(:,x,y,z)=0;
                
            elseif (x~=1)&&abs(mean(delta(:,x,y,z))-mean(dX(find(dX))))>(std(dX(find(dX)))/2)
                delta(:,x,y,z)=0;              
            end
        end
    end
end

% %(x==1)&&(y~=1)
a1=sum(sum(sum(delta(1,1,2:end,:))))/length(find(delta(1,1,2:end,:)));
a2=sum(sum(sum(delta(2,1,2:end,:))))/length(find(delta(2,1,2:end,:)));
a3=sum(sum(sum(delta(3,1,2:end,:))))/length(find(delta(3,1,2:end,:)));

% %(x~=1)
b1=sum(sum(sum(delta(1,2:end,:,:))))/length(find(delta(1,2:end,:,:)));
b2=sum(sum(sum(delta(2,2:end,:,:))))/length(find(delta(2,2:end,:,:)));
b3=sum(sum(sum(delta(3,2:end,:,:))))/length(find(delta(3,2:end,:,:)));

for z = 1:size(delta,4)
    for y = 1:size(delta,3)
        for x = 1: size(delta,2)
            if (x==1)&&(y~=1)&&(sum(delta(:,x,y,z),1) < 100)
                delta(1,x,y,z) = a1;
                delta(2,x,y,z) = a2;
                delta(3,x,y,z) = a3;
                
            elseif (x~=1)&&(sum(delta(:,x,y,z),1) < 100)
                delta(1,x,y,z) = b1;
                delta(2,x,y,z) = b2;
                delta(3,x,y,z) = b3;
                
            end
        end
    end
end


delta = round(delta);
%save([directory 'delta.mat'],'delta');


