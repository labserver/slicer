function bin2mat (directory, frameX, frameY, frameZ)
%fname : directory and file name
for z = 1:frameZ
    for y = 1:frameY
        for x = 1:frameX
            tic
            fid = fopen([directory '\RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.bin'], 'rb');
            img = fread(fid, inf, 'uint16');
            img = reshape(img,[1152 512 512]);
            save([directory '\RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'img','-v7.3');
            fclose('all');
            delete([directory '\RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.bin']);
            toc
        end
    end
end
