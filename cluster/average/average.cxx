/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    itkSampleProject.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$

  Copyright (c) 2002 Insight Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include <cmath>
#include <stdio.h>
#include <dirent.h>
#include <string>
#include <vector>
#include "itkTimeProbe.h"

int main (  int argc, char* argv[] );
std::vector<std::string> list_files( const char* );


/******************************************************************************/

int main ( int argc, char* argv[] )
{
	FILE* fp;
	short int* read_buffer;
	float* mean_buffer;
	int nx,ny,nz,npts;
	int i,i_file;

	nx=512;
	ny=512;
	nz=200;
	npts = nx * ny * nz;

	itk::TimeProbe clock;
	clock.Start();
	std::vector<std::string> file_list = list_files(argv[1]);

	read_buffer = ( short int * ) malloc ( sizeof ( short int ) * nx * ny * nz );
	mean_buffer = ( float * ) calloc ( nx * ny * nz,  sizeof ( float ) );

	// Read and add
	for(i_file=0;i_file<file_list.size();i_file++)
	{
		fp = fopen(file_list[i_file].c_str(), "rb");
		fseek(fp, SEEK_SET, 0);
		fread(read_buffer, sizeof(short int) * nx * ny * nz, 1, fp);
		fclose(fp);
		for ( int i = 0; i < npts; i++ )
		{
			mean_buffer[i] += read_buffer[i];
		}
	}
	// Normalize and recast
	for ( int i = 0; i < npts; i++ )
	{
		mean_buffer[i] /= file_list.size();
		read_buffer[i] = (short int) mean_buffer[i];
	}
	// Save
	std::string file_out = std::string(argv[1])+"/volume.ave";
	fp = fopen(file_out.c_str(), "wb");
	fwrite(read_buffer, sizeof(short int) * nx * ny * nz, 1, fp);
	fclose(fp);
	clock.Stop();

	std::cout << "Total: " << clock.GetTotal()<< std::endl;

	free(mean_buffer);
	free(read_buffer);
	return 0;
}

std::vector<std::string> list_files( const char* path )
{
	std::vector<std::string> file_list;
	DIR* dirFile = opendir( path );
	if ( dirFile )
	{
		struct dirent* hFile;
		errno = 0;
		while (( hFile = readdir( dirFile )) != NULL )
		{
			if ( !strcmp( hFile->d_name, "."  )) continue;
			if ( !strcmp( hFile->d_name, ".." )) continue;

			// dirFile.name is the name of the file. Do whatever string comparison
			// you want here. Something like:
			if ( strstr( hFile->d_name, ".bin" ))
			{
				file_list.push_back(std::string(path)+"/"+std::string(hFile->d_name));
			}
		}
		closedir( dirFile );
	}
	return file_list;
}
