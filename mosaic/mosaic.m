function  mosaic(directory, frameX, frameY, frameZ, pixelI, pixelF)
%Function to create mosaic images of selected pixels of all slices

load([directory 'meanVolume.mat']);
w= 5;
for z = 1:frameZ
    
    for px = 1:(pixelF-pixelI+1)/w;
        tic
        load([directory 'delta.mat']);
        delta(1,2:end,:,:)=-delta(1,2:end,:,:);
        delta(:,1,1,z) = [0; 0; 0];
        for y = 1:frameY
            if y~=1
                delta(:,1,y,z)=delta(:,1,y,z)-(sum(delta(:,2:round(frameX/2),y,z),2)-sum(delta(:,2:round(frameX/2),y-1,z),2));
            end
            for x = 1:frameX
                fid = fopen([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'rb');
                volume = fread(fid,inf, 'uint16');
                volume = reshape(volume,[200 512 512]);
                fclose(fid);
               % volume = volume./sumV;
                
                for i = 1:size(volume,1)
                    meanS(i) = sum(sum(volume(i,:,:)));
                end
                derivative=diff(meanS);
                maxDiff = find(derivative==max(derivative(1:40)));
                A = find(meanS==max(meanS(1:maxDiff+5)));
               
                a = 512+sum(delta(2,1:x,y,z))+sum(delta(2,1,1:(y-1),z));
                b = 512+sum(delta(3,1:x,y,z))+sum(delta(3,1,1:(y-1),z));
                
                %m(:,a+1:a+size(volume,2),b+1:b+size(volume,3)) = volume(pixelI+(px-1)*w-sum(delta(1,1:x,y,z))-sum(delta(1,1,1:(y-1),z)):pixelI+(px-1)*w+(w-1)-sum(delta(1,1:x,y,z))-sum(delta(1,1,1:(y-1),z)),:,:);
                m(:,a+1:a+size(volume,2),b+1:b+size(volume,3)) = volume(pixelI+(px-1)*w+A:pixelI+(px-1)*w+(w-1)+A,:,:);
                if (x==1)&&(y==1)
                    M(:,1:size(m,2),1:size(m,3),1) = m;
                    clear m
                else
                    M(:,1:size(m,2),1:size(m,3),2) = m;
                    clear m
                    M(:,:,:,3) = sum(M,4);%./max(1,sum(M(:,:,1:2)~=0,3));
                    
                    if x~=1
                        L1 = repmat(linspace(0,1,512-delta(2,x,y,z)),[min(512,512-delta(3,x,y,z)),1])';
                        L2 = repmat(linspace(1,0,512-delta(2,x,y,z)),[min(512,512-delta(3,x,y,z)),1])';
                        M(:,a+1:a+(512-delta(2,x,y,z)),b+1:b+ min(512-delta(3,x,y,z),512),3)...
                            = M(:,a+1:a+(512-delta(2,x,y,z)),b+1:b+min(512-delta(3,x,y,z),512),1).*reshape(meshgrid(L2,1:size(M,1)),[size(M,1) size(L2,1) size(L2,2)])...
                            +M(:,a+1:a+(512-delta(2,x,y,z)),b+1:b+min(512,512-delta(3,x,y,z)),2).*reshape(meshgrid(L1,1:size(M,1)),[size(M,1) size(L1,1) size(L1,2)]);
                    end
                    if y~=1
                        d = min(512+sum(delta(2,1:x,y,z))+sum(delta(2,1,1:(y-1),z))+512,512+1+sum(delta(2,1:x,y-1,z))+sum(delta(2,1,1:(y-2),z))+512);
                        e = 512+1+sum(delta(3,1:x,y,z))+sum(delta(3,1,1:(y-1),z));
                        f = 512+sum(delta(3,1:x,y-1,z))+sum(delta(3,1,1:(y-2),z))+512;
                        if x~=1
                            c=a+(512-delta(2,x,y,z))+1;
                            M(:,c:sum(delta(2,2:x-1,y-1,z))+sum(delta(2,1,y-1,z))+1024,f+1:sum(delta(3,2:x-1,y-1,z))+sum(delta(3,1,y-1,z))+1024,3)...
                                =(M(:,c:sum(delta(2,2:x-1,y-1,z))+sum(delta(2,1,y-1,z))+1024,f+1:sum(delta(3,2:x-1,y-1,z))+sum(delta(3,1,y-1,z))+1024,1)...
                                +M(:,c:sum(delta(2,2:x-1,y-1,z))+sum(delta(2,1,y-1,z))+1024,f+1:sum(delta(3,2:x-1,y-1,z))+sum(delta(3,1,y-1,z))+1024,2))/2;
                        else
                            c = max(512+1+sum(delta(2,1:x,y,z))+sum(delta(2,1,1:(y-1),z)),512+1+sum(delta(2,1:x,y-1,z))+sum(delta(2,1,1:(y-2),z)));
                        end

                        s = size( M(:,c:d,e:f,3));
                        L3 = repmat(linspace(0,1,s(3)),[s(2),1]);
                        L4 = repmat(linspace(1,0,s(3)),[s(2),1]);
                        
                        
                        M(:,c:d,e:f,3) = M(:,c:d,e:f,1).*reshape(meshgrid(L4,1:size(M,1)),[size(M,1) size(L4,1) size(L4,2)]) + M(:,c:d,e:f,2).*reshape(meshgrid(L3,1:size(M,1)),[size(M,1) size(L3,1) size(L3,2)]);
                        
                        
                    end
                    
                    M(:,:,:,1) = M(:,:,:,3);%load('/media/DataFred/CerveauSouris/meanVolume.mat');
                    M = M(:,:,:,1);
                end
            end
        end
         for i = 1 : size(M,1)
             slice = M(i,:,:);
             save([directory 'slices/slice' sprintf( '%03.0f', z) '_pixel' sprintf( '%03.0f', i+size(M,1)*(px-1))  '.mat'],'slice');
             clear slice
         end
         clear M
        toc
    end
end