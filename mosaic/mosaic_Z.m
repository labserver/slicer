function mosaic_Z(directory,nslice, npixel)
%Recollage de tous les slices
load([directory 'MemMapFileInfo.mat'])
load([directory 'deltaM.mat'])
m=memmapfile([directory 'MemMapFile.mat'], 'Format', {'uint16' [info.x info.y info.z] 'x'}, 'Writable', true);

 for i=1:info.z
S(i)= mean(mean(m.Data(1).x(1000:2500,1500:3500,i)));
 end

% delta(1,:)=delta(1,:).*info.y/1024;
% delta(3,:)=delta(3,:).*info.x/1024;
deltaT=round(delta);
delta(2,:)=40-deltaT(3,:);
delta(1,:)=deltaT(2,:);
delta(3,:)=-deltaT(1,:);


for i = 1:nslice
  tic
    for j = 1:npixel
        M=zeros(info.x+300,info.y+300);
        if i == 1
            L1 = [1:-1/(npixel-delta(2,i)):0];
            L2 = [0:1/(npixel-delta(2,i)):1];
            z1=100;
            z2=z1;
            z3=100+delta(3,i);
            z4=100+delta(1,i);
            if j < delta(2,i)
                M(z1+1:z1+info.x,z2+1:z2+info.y) = m.Data(1).x(:,:,j+(i-1)*npixel).*(mean(S)/S(j+(i-1)*npixel));
%                 M = M/mean(mean(M));
%                 M = M*((2^16)/max(max(M)));
                M = uint16(M);
            elseif j >= delta(2,i)
                M(z1+1:z1+info.x,z2+1:z2+info.y,1) = m.Data(1).x(:,:,j+(i-1)*npixel)*L1(j-delta(2,i)+1).*(mean(S)/S(j+(i-1)*npixel));
                M(z3+1:z3+info.x,z4+1:z4+info.y,2) = m.Data(1).x(:,:,1+(j-delta(2,i)+i*npixel))*L2(j-delta(2,i)+1).*(mean(S)/S(1+(j-delta(2,i)+i*npixel)));
                M = M(:,:,1)+M(:,:,2);
%                 M = M/mean(mean(M));
%                 M = M*((2^16)/max(max(M)));
                M = uint16(M);
            end
            if j==1
                fid = fopen([directory 'final.mat'], 'w');
                fwrite(fid,M, 'uint16');
            elseif j>1
                fid = fopen([directory 'final.mat'], 'a');
                fwrite(fid,M, 'uint16');
            end
        elseif (i>1)&(i<nslice)
            L1 = [1:-1/(npixel-delta(2,i)):0].^2;
            L2 = -([-1:1/(npixel-delta(2,i)):0].^2)+1;
            z1=100+sum(delta(3,1:i-1));
            z2=100+sum(delta(1,1:i-1));
            z3=100+sum(delta(3,1:i));
            z4=100+sum(delta(1,1:i));
            if (j>(npixel-delta(2,i-1)+1))&(j<delta(2,i))
                M(z1+1:z1+info.x,z2+1:z2+info.y) = m.Data(1).x(:,:,j+(i-1)*npixel).*(mean(S)/S(j+(i-1)*npixel));
%                 M = M/mean(mean(M));
%                 M = M*((2^16)/max(max(M)));
                M = uint16(M);
                fid = fopen([directory 'final.mat'], 'a');
                fwrite(fid,M, 'uint16');
            elseif j >= delta(2,i)
                M(z1+1:z1+info.x,z2+1:z2+info.y,1) = m.Data(1).x(:,:,j+(i-1)*npixel)*L1(j-delta(2,i)+1).*(mean(S)/S(j+(i-1)*npixel));
                M(z3+1:z3+info.x,z4+1:z4+info.y,2) = m.Data(1).x(:,:,1+(j-delta(2,i)+i*npixel))*L2(j-delta(2,i)+1).*(mean(S)/S(1+(j-delta(2,i)+i*npixel)));
                M = M(:,:,1)+M(:,:,2);
%                 M = M/mean(mean(M));
%                 M = M*((2^16)/max(max(M)));
                M = uint16(M);
                fid = fopen([directory 'final.mat'], 'a');
                fwrite(fid,M, 'uint16');
            end
        elseif i==nslice
            z1=100+sum(delta(3,1:i-1));
            z2=100+sum(delta(1,1:i-1));
            if j>(npixel-delta(2,i-1)+1)
                M(z1+1:z1+info.x,z2+1:z2+info.y) = m.Data(1).x(:,:,j+(i-1)*npixel).*(mean(S)/S(j+(i-1)*npixel));
%                 M = M/mean(mean(M));
%                 M = M*((2^16)/max(max(M)));
                M = uint16(M);
                fid = fopen([directory 'final.mat'], 'a');
                fwrite(fid,M, 'uint16');
            end
            
        end
    end
    toc
end


            
            
        




