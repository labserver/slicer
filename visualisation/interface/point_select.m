function varargout = point_select(varargin)
% POINT_SELECT MATLAB code for point_select.fig
%      POINT_SELECT, by itself, creates a new POINT_SELECT or raises the existing
%      singleton*.
%
%      H = POINT_SELECT returns the handle to a new POINT_SELECT or the handle to
%      the existing singleton*.
%
%      POINT_SELECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in POINT_SELECT.M with the given input arguments.
%
%      POINT_SELECT('Property','Value',...) creates a new POINT_SELECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before point_select_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to point_select_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help point_select

% Last Modified by GUIDE v2.5 22-Jan-2014 13:35:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @point_select_OpeningFcn, ...
                   'gui_OutputFcn',  @point_select_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before point_select is made visible.
function point_select_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to point_select (see VARARGIN)

% Choose default command line output for point_select
handles.output = hObject;
set(handles.cAtlas,'Value',1)
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes point_select wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = point_select_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pLoad.
function pLoad_Callback(hObject, eventdata, handles)
% hObject    handle to pLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.folder = uigetdir;
load([handles.folder '/deltaM.mat'])
load([handles.folder '/MemMapFileInfo.mat']);
info.x=info.x+300;
info.y=info.y+300;
info.z=info.z-sum(round(delta(3,:)))-size(delta,2);%+1+1;

if(handles.folder)
    
    % Verify if reduced version is already computed
    tmp=dir([handles.folder '/iso_volume.mat']);
    if(size(tmp,1)==0)
        % Need to do the image subsampling (careful with ordering)
        f=memmapfile([handles.folder '/final.mat'], 'Format', {'uint16' [info.x info.y info.z] 'x'}, 'Writable', true);
        factorXY = (1/154)/(1/512);
        factorZ = 1;
        tmp_volume=zeros(round(info.x/factorXY),round(info.y/factorXY),info.z);
        if size(tmp_volume,1)*size(tmp_volume,2)*size(tmp_volume,3) > 512*512*512
            factorXY = factorXY*2;
            factorZ = factorZ*2;
            tmp_volume=zeros(round(info.x/factorXY),round(info.y/factorXY),info.z);
        end
        for i=1:info.z
            big_img=squeeze(f.Data(1).x(:,:,i));
            tmp_volume(:,:,i) = imresize(big_img,[round(info.x/factorXY),round(info.y/factorXY)]);
        end
        % We reduced in x and y, need to reduce in z...
        iso_volume=zeros(round(info.x/factorXY),round(info.y/factorXY),info.z/factorZ);
        for i=1:round(info.x/factorXY)
            iso_volume(i,:,:)=imresize(squeeze(tmp_volume(i,:,:)),[round(info.y/factorXY),info.z/factorZ]);
        end
        clear tmp_volume;
        save([handles.folder '/iso_volume.mat'],'iso_volume');
        save([handles.folder '/factorZ.mat'],'factorZ');
    else
        % Load Reduced version
        
        load([handles.folder '/iso_volume.mat']);
    end
    handles.Data=iso_volume;
    clear iso_volume
    % LOAD NIFTI
    tmp = dir([handles.folder '/nifti_vol.mat']);
    if (size(tmp,1)==0)
    nifti=load_nii('/home/vibratome/pouf.nii');
    for i=1:548
    nifti_vol(:,:,i)=fliplr(rot90(squeeze(nifti.img(:,549-i,:))));
    end
    save([handles.folder '/nifti_vol'], 'nifti_vol')
    else
        load([handles.folder '/nifti_vol.mat']);
    end
    handles.nifti=nifti_vol;
    
    clear nifti_vol
    
    % Update Slider
    %sData
    set(handles.sData,'Max',size(handles.Data,3))
    set(handles.sData,'Min',1)
    set(handles.sData, 'Value', size(handles.Data,3)/2);
    set(handles.sData, 'SliderStep', [1/(size(handles.Data,3)-1) 1/(size(handles.Data,3)-1)]);
    %sAtlas
    set(handles.sAtlas,'Max',548)
    set(handles.sAtlas,'Min',1)
    set(handles.sAtlas, 'Value', 548/2);
    set(handles.sAtlas, 'SliderStep', [1/547 1/547]);
    
    % Update display
    axes(handles.aAtlas)
    imagesc(squeeze(handles.nifti(:,:,end/2))); colormap 'gray'; axis image
    set(handles.tAtlas,'String', num2str(round(get(handles.sAtlas,'Value'))));  
   % set(gca,'DataAspectRatio',[info.x/info.y info.y/info.y 1])
    axis off;
    f =get(handles.aAtlas,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,1})
    axes(handles.aData)
    imagesc(squeeze(handles.Data(:,:,end/2))); colormap 'gray'; axis image
    set(handles.tData,'String', num2str(round(get(handles.sData,'Value'))));
    %set(gca,'DataAspectRatio',[info.x/info.y (info.z*512/154)/info.y 1])
    axis off;
    f =get(handles.aData,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,2})
        
    % Update handles structure
    guidata(hObject, handles);
end


% --- Executes on slider movement.
function sData_Callback(hObject, eventdata, handles)
% hObject    handle to sData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
axes(handles.aData)
    imagesc(squeeze(handles.Data(:,:,round(get(handles.sData,'Value'))))); colormap 'gray'; axis image
    set(handles.tData,'String', num2str(round(get(handles.sData,'Value'))));  
   % set(gca,'DataAspectRatio',[info.x/info.y info.y/info.y 1])
    axis off;

% --- Executes during object creation, after setting all properties.
function sData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sAtlas_Callback(hObject, eventdata, handles)
% hObject    handle to sAtlas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
axes(handles.aAtlas)
    imagesc(squeeze(handles.nifti(:,:,round(get(handles.sAtlas,'Value'))))); colormap 'gray'; axis image
    set(handles.tAtlas,'String', num2str(round(get(handles.sAtlas,'Value'))));  
   % set(gca,'DataAspectRatio',[info.x/info.y info.y/info.y 1])
    axis off;

% --- Executes during object creation, after setting all properties.
function sAtlas_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sAtlas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in pSelect.
function pSelect_Callback(hObject, eventdata, handles)
% hObject    handle to pSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[a,b]=cpselect(squeeze(handles.nifti(:,:,round(get(handles.sAtlas,'Value'))))./max(max(max(handles.nifti(:,:,round(get(handles.sAtlas,'Value')))))), squeeze(handles.Data(:,:,round(get(handles.sData,'Value'))))./max(max(max(handles.Data(:,:,round(get(handles.sData,'Value')))))), 'Wait', true);
a(:,3)=round(get(handles.sAtlas,'Value'));
b(:,3)=round(get(handles.sData,'Value'));
if isfield(handles,'A')
    handles.A(size(handles.A,1)+1:size(handles.A,1)+size(a,1),:) = a;
else
    handles.A=a; 
end
if isfield(handles,'B')
    handles.B(size(handles.B,1)+1:size(handles.B,1)+size(b,1),:) = b;
else
     handles.B=b;
end
set(handles.tNbPoints,'String', num2str(size(handles.A,1)));
    % Update handles structure
    guidata(hObject, handles);


% --- Executes on button press in pWarp.
function pWarp_Callback(hObject, eventdata, handles)
% hObject    handle to pWarp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load([handles.folder '/factorZ.mat']);
if get(handles.cAtlas,'Value')
    [warpedImage,TransformMatrix] = registration(handles.nifti, handles.Data, [0.03 0.03 0.03], [factorZ/154 factorZ/154 factorZ/154], handles.A, handles.B );
    save([handles.folder '/warpedData.mat'], 'warpedImage')
    save([handles.folder '/DataTransformMatrix.mat'], 'TransformMatrix')
end
if get(handles.cData,'Value')
    [warpedImage,TransformMatrix] = registration(handles.Data, handles.nifti, [factorZ/154 factorZ/154 factorZ/154], [0.03 0.03 0.03], handles.B, handles.A );
    save([handles.folder '/warpedAtlas.mat'], 'warpedImage')
    save([handles.folder '/AtlasTransformMatrix.mat'], 'TransformMatrix')
end




% --- Executes on button press in cAtlas.
function cAtlas_Callback(hObject, eventdata, handles)
% hObject    handle to cAtlas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cAtlas
%set(handles.cData,'Value',0)

% --- Executes on button press in cData.
function cData_Callback(hObject, eventdata, handles)
% hObject    handle to cData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cData
%set(handles.cAtlas,'Value',0)