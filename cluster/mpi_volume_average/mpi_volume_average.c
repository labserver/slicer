#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "mpi.h"

#define WORKTAG 1

/* Local functions */
static void master(const char*);
static void slave(void);

int main(int argc, char **argv)
{
	int myrank;

	/* Initialize MPI */
	MPI_Init(&argc, &argv);

	/* Find out my identity in the default communicator */
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	if (myrank == 0) {
		master(argv[1]);
	} else {
		slave();
	}

	/* Shut down MPI */
	MPI_Finalize();
	return 0;
}


static void master(const char* dir)
{
	int ntasks, rank;
	int n_files, n_files_per_node;
	int local_sum, total_sum;
	MPI_Status status;

	DIR *dp;
	struct dirent *entry;
	struct stat statbuf;


	/* Find out how many processes there are in the default
     communicator */
	MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

	/* Read directory and find each file. */
	if((dp = opendir(dir)) == NULL) {
		fprintf(stderr,"cannot open directory: %s\n", dir);
		return;
	}
	chdir(dir);
	rank = 0;
	while((entry = readdir(dp)) != NULL)
	{
		lstat(entry->d_name,&statbuf);
		if(!S_ISDIR(statbuf.st_mode)) {
			/* Found a file, ignore directories */
			printf("%s\n",entry->d_name);
			rank = (rank+1)%ntasks;
			/* Send it to each rank */
			MPI_Send(entry->d_name,             			/* message buffer */
					strlen(entry->d_name),                 	/* number of data item per node */
					MPI_CHAR,           					/* data item is an integer */
					rank,              						/* destination process rank */
					WORKTAG,           						/* user chosen message tag */
					MPI_COMM_WORLD);   						/* default communicator */
		}
	}
	chdir("..");
	closedir(dp);

	/* There's no more work to be done, so receive all the outstanding
     results from the slaves. */
	total_sum=0;
	for (rank = 1; rank < ntasks; ++rank) {
		MPI_Recv(&local_sum, 1, MPI_INT, MPI_ANY_SOURCE,
				MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		total_sum+=local_sum;
	}
}


static void slave(void)
{
	MPI_Status status;
	int data;
	int result;

	/* Receive a message from the master */
	MPI_Recv(&data, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

	/* Do the work */
	result=data;
	printf("here");
	/* Send the result back */
	MPI_Send(&result, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
}


