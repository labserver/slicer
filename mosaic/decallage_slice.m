function [delta]=decallage_slice
for iz = 1:18
    for ip = 1:10
        a=load(['/home/vibratome/CerveauSouris/' 'slice' sprintf( '%03.0f', iz) '_pixel' sprintf( '%03.0f', ip+30) '.mat']);
        img1 = squeeze(a.slice(1,430:5300,700:4080));
        f1 = fftshift(fft2(img1));
        f1(2436-5:2436+5,1691-5:1691+5)=0;
        f1 = abs(ifft2(f1));
        f1 = filtrage(f1,1);
        a=load(['/home/vibratome/CerveauSouris/' 'slice' sprintf( '%03.0f', iz+1) '_pixel' sprintf( '%03.0f', ip) '.mat']);
        img2 = squeeze(a.slice(1,430:5300,700:4080));
        f2 = fftshift(fft2(img2));
        f2(2436-5:2436+5,1691-5:1691+5)=0;
        f2 = abs(ifft2(f2));
        f2 = filtrage(f2,1);
        
        [deltaX , deltaY] = ExtPhaseCorrelation(f1, f2);
        delta(1,ip,iz) = round(deltaX);
        delta(2,ip,iz) = round(deltaY);
    end
end