%% Filtrage
function [fM] = filtrage(M, f)


%Filtre gaussien
    f = fspecial('gaussian',5*f,f);



fM(:,:) = conv2(M(:,:),f,'same')./conv2(ones(size(M(:,:))),f,'same');