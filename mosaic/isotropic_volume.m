f=memmapfile('/media/Aging/CerveauSouris/slices_sameShift/final.mat', 'Format', {'uint16' [3681 5171 640] 'x'});

sizeX = size(f.Data(1).x,1);
sizeY = size(f.Data(1).x,2);
sizeZ = size(f.Data(1).x,3);

factor = (1/154)/(1/512); % (Z pixel size)/ (XY pixel size)

iso_volume=zeros(round(sizeX/factor),round(sizeY/factor),sizeZ);

for i=1:sizeZ
    img=squeeze(f.Data(1).x(:,:,i));
    iso_volume(:,:,i) = imresize(img,[round(sizeX/factor),round(sizeY/factor)]);
end
