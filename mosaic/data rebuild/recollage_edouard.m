close all
Delta=zeros(2,511);
% for u=1:1  
% %cr�ation de la premi�re image
% fftAA=fft(AA(:,:,u),2048,1); % read image 1 
% image1 = fftshift(fftAA(end/2:-1:1,:),1);
% figure; imshow(log(image1),[]);
% image1=image1(1:1024, 51:512); %on enl�ve le recouvrement
% size(image1)
% %cr�ation de la deuxi�me image
% fftAA2= fft(AA(:,:,u+1),2048,1); % read image 2 must have same dimensions as image 1
% image2=fftshift(fftAA2(end/2:-1:1,:),1);
% image2=image2(1:1024, 51:512);%on enl�ve le recouvrement
% size(image2)
image1 = gauche;
image2 = droite;
figure; imshow(log(image1),[]);
figure; imshow(log(image2),[]);
%filtrage
filter1=hamming(size(image1,1)); % setup window in one direction - I chose Blackman window - could use Hamming or others
filter2=hamming(size(image1,2)); % setup window in second direction
filtimg1=(filter1*filter2'); % make 2D windowing function that is sized to match image
Image1=double(image1).*filtimg1; % apply windowing function to image
%figure; imshow(log(Image1),[]);
Image2=double(image2).*filtimg1;
%figure; imshow(log(Image2),[]);

%calcul de la corr�lation de phase
FFT1 = fft2(image1); % 2d FFT
FFT2 = conj(fft2(image2)); 
FFTR = FFT1.*FFT2;
magFFTR = abs(FFTR);
FFTRN = (FFTR./magFFTR);
result = ifft2(double(FFTRN)); 
figure;
colormap('gray'); % choose colormap for plotting
imagesc(abs(result));
figure;
colormap(jet);
mesh(abs(result));

mmax = max(max(abs(result)));
%Find the main peak
[x,y,v] = find(mmax == abs(result));

%Extension to Subpixel Registration [Foroosh, Zerubia & Berthod, 2002]
%============================================================
[M, N] = size(Image1);

%two side-peaks
xsp = x + 1;
xsn = x - 1;
ysp = y + 1;
ysn = y - 1;


% %if the peaks outsize the image, then use xsn and/or ysn for the two
% %side-peaks
if xsp > M
    xsp = M-1;
end
if ysp > N
    ysp = N-1;
end
% %Calculate the translational shift

deltaX1=((result(xsp,y) * xsp + result(x,y) * x) / (result(xsp,y) + result(x,y)))-1;
deltaY1=((result(x,ysp) * ysp + result(x,y) * y) / (result(x,ysp) + result(x,y)))-1;


% %I don't know why but after few test i find out that the result of deltaX
% %and delta Y is inverted.. :( ??
% 
% %Validate if translation shift is negative
if deltaX1 < (N/2)
    deltaY = deltaX1;
else
    deltaY = deltaX1 - M;
end

if deltaY1 < (M/2)
    deltaX = deltaY1;
else
    deltaX = deltaY1 - N;
end

% Delta(1,u)= deltaX;
% Delta(2,u)= deltaY;

%  end