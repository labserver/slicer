function [warpedImage,TransformMatrix] = registration(fixedVolume, movingVolume, fixedSpacing, movingSpacing, fixedPoints, movingPoints )

%http://www.mathworks.com/help/images/examples/registering-multimodal-3-d-medical-images.html
% Volumes are expected to be 3D matrices from each image we want to
% register. Fixed should be the frame of reference (e.g. the template)
% Spacing variables define the size of voxels in a given unit (i.e.
% dx,dy,dz)
% Points are (x,y,z)-(u,v,w) point pairs that are in the same phyical
% location

% First step: Get manual point between fixed and moving image
% We know that an affine transformation will map fixed (x,y,z) to warped
% (u,v,w) space through: (x,y,z,1)=(u,v,w,1)*T where T is the transform
% matrix we are looking for. Then if you identify a set of 'n' points in
% both coordinate systems, you have:
% (x1,y1,z1,1)=(u1,v1,w1,1)*T
% (x2,y2,z2,1)=(u2,v2,w2,1)*T
% (x3,y3,z3,1)=(u3,v3,w3,1)*T
% (x4,y4,z4,1)=(u4,v4,w4,1)*T
% (x5,y5,z5,1)=(u5,v5,w5,1)*T
% ...
% Call the nx4 matrix on the left A, and the nx4 matrix on the right B.
% Then the transform matrix is given by:
% A=B*T -> T=B^-1*A = pinv(B)*A;


% Find reference
Rfixed  = imref3d(size(fixedVolume),fixedSpacing(2),fixedSpacing(1),fixedSpacing(3));
%Rmoving  = imref3d(size(fixedVolume),movingSpacing(2),movingSpacing(1),movingSpacing(3));
Rmoving  = imref3d(size(movingVolume),movingSpacing(2),movingSpacing(1),movingSpacing(3));

% First extend points to create transform vectors
A = [fixedPoints*fixedSpacing(1),ones(size(fixedPoints,1),1)];
B = [movingPoints*movingSpacing(1),ones(size(movingPoints,1),1)];

% Compute the transform T:
t=affine3d();
t.T=round(pinv(B)*A*1000)/1000;


% Then we can map any point from one space to the other. There is a
% facility function to compute the image (u,v,w) in space (x,y,z)

warpedImage = imwarp(movingVolume,Rmoving,t,'cubic','OutputView',Rfixed);
TransformMatrix = t.T;