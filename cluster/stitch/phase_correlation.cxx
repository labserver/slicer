/*=========================================================================

  Program:   Insight Segmentation & Registration Toolkit
  Module:    itkSampleProject.cxx
  Language:  C++
  Date:      $Date$
  Version:   $Revision$

  Copyright (c) 2002 Insight Consortium. All rights reserved.
  See ITKCopyright.txt or http://www.itk.org/HTML/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include <cmath>
#include <stdio.h>
#include "itkTimeProbe.h"
#include <fftw3.h>

int main ( );
void phase_correlation( double* in1, double* in2, int nx, int ny, int nz, int& ix, int& iy, int& iz );


/******************************************************************************/

int main ( )
{
	double *in1,*in2;
	short int* read_buffer;
	int nx,ny,nz;
	nx=512;
	ny=512;
	nz=200;
	itk::TimeProbe clock;
	clock.Start();

	read_buffer = ( short int * ) malloc ( sizeof ( short int ) * nx * ny * nz );

	FILE* fp = fopen("volume_x06_y06_z30.bin", "rb");
	fseek(fp, SEEK_SET, 0);
	fread(read_buffer, sizeof(short int) * nx * ny * nz, 1, fp);
	fclose(fp);
	int nzt=nz/2;
	int nyt=ny/4;
	in1 = ( double * ) malloc ( sizeof ( double ) * nx * nyt * nzt );
	for ( int i = 0; i < nx; i++ )
	{
		for ( int j = 0; j < nyt; j++ )
		{
			for( int k=0; k< nzt; k++ )
			{
				in1[k+nzt*(j+i*nyt)] = read_buffer[k+nz*((ny-nyt+j)+i*ny)];
			}
		}
	}

	fp = fopen("volume_x07_y06_z30.bin", "rb");
	fseek(fp, SEEK_SET, 0);
	fread(read_buffer, sizeof(short int) * nx * ny * nz, 1, fp);
	fclose(fp);
	in2 = ( double * ) malloc ( sizeof ( double ) * nx * nyt * nzt );
	for ( int i = 0; i < nx; i++ )
	{
		for ( int j = 0; j < nyt; j++ )
		{
			for( int k=0; k< nzt; k++ )
			{
				in2[k+nzt*(j+i*nyt)] = read_buffer[k+nz*(j+i*ny)];
			}
		}
	}


	int ix,iy, iz;
	phase_correlation( in1, in2, nx, nyt, nzt, ix, iy, iz);
	clock.Stop();
	std::cout << "Total: " << clock.GetTotal()<< std::endl << "posx: " << ix << " posy: " << iy+(ny-nyt) << " posz: " << iz << std::endl;


	free(in1);
	free(in2);
	free(read_buffer);
	return 0;
}




void phase_correlation( double* in1, double* in2, int nx, int ny, int nz, int& ix, int& iy, int& iz )
{
	int nzh;
	fftw_complex *out1, *out2;
	double norm;
	fftw_plan plan_backward;
	fftw_plan plan_forward1;
	fftw_plan plan_forward2;
	nzh = ( nz / 2 ) + 1;
	out1 = (fftw_complex*) fftw_malloc ( sizeof ( fftw_complex ) * nx * ny *nzh );

	plan_forward1 = fftw_plan_dft_r2c_3d ( nx, ny, nz, in1, out1, FFTW_ESTIMATE );
	fftw_execute ( plan_forward1 );
	out2 = (fftw_complex*) fftw_malloc ( sizeof ( fftw_complex ) * nx * ny * nzh );
	plan_forward2 = fftw_plan_dft_r2c_3d ( nx, ny, nz, in2, out2, FFTW_ESTIMATE );
	fftw_execute ( plan_forward2 );

	for(int i=0; i<nx*ny*nzh; i++){
		norm = sqrt( (out1[i][0]*out2[i][0] + out1[i][1]*out2[i][1])*(out1[i][0]*out2[i][0] + out1[i][1]*out2[i][1]) +
				(out1[i][1]*out2[i][0] - out1[i][0]*out2[i][1])*(out1[i][1]*out2[i][0] - out1[i][0]*out2[i][1]) );
		out1[i][0] = (out1[i][0]*out2[i][0] + out1[i][1]*out2[i][1])/norm;
		out1[i][1] = (out1[i][1]*out2[i][0] - out1[i][0]*out2[i][1])/norm;
	}
	plan_backward = fftw_plan_dft_c2r_3d ( nx, ny, nz, out1, in2, FFTW_ESTIMATE );
	fftw_execute ( plan_backward );

	// Find the maximum position
	double max = 0.0;
	for (int i=0;i<nx;i++)
	{
		for(int j=0;j<ny;j++)
		{
			for(int k=0;k<nz;k++)
			{
				if(in2[k+nz*(j+i*ny)]>max)
				{
					max=in2[k+nz*(j+i*ny)];
					// X and Y interverted with respect to scan
					ix=i;
					iy=j;
					iz=k;
				}
			}
		}
	}
	// Correct for negative shift for x
	if (ix >= (nx/2)) ix = ix - nx;
	if (iy>= (ny/2)) iy = iy - ny;
	if (iz >= (nz/2)) iz = iz - nz;

	// Free up the allocated memory.
	fftw_destroy_plan ( plan_forward1 );
	fftw_destroy_plan ( plan_forward2 );
	fftw_destroy_plan ( plan_backward );
	fftw_free ( out1 );
	fftw_free ( out2 );

	return;
}
