function shift_mosaic(directory,nslice, npixel)
% Calcul le décallage entre les différentes slices
load([directory 'MemMapFileInfo.mat'])
info.z=600;
f=memmapfile([directory 'MemMapFile.mat'], 'Format', {'uint16' [info.x info.y info.z] 'x'}, 'Writable', true);

tmp_volume=zeros(1024,1024,info.z);
for i=1:info.z
    big_img=squeeze(f.Data(1).x(:,:,i));
    tmp_volume(:,:,i) = imresize(big_img,[1024,1024]);
end


for i = 1 : nslice-1
    tic
    z = npixel*(i-1);
    
    img1 = tmp_volume(:,:,z+1:z+npixel);
    img2 = tmp_volume(:,:,z+1+npixel:z+2*npixel);
    [deltaX1 , deltaY1, deltaZ1] = ExtPhaseCorrelation3D(img1, img2);
    delta(:,i) = [deltaX1 , deltaY1, deltaZ1];
end
%delta = round(delta);
for i = 1:nslice-1
    if sum(delta(:,i)) < 10
        delta(:,i) = 0;
    end
end
for i=1:nslice-1
    if sum(delta(:,i))==0
       delta(1,i) = sum(delta(1,:))/length(find(delta(1,:)));
       delta(2,i) = sum(delta(2,:))/length(find(delta(2,:)));
       delta(3,i) = sum(delta(3,:))/length(find(delta(3,:)));
    end
end



save([directory 'delta.mat'], 'delta')
