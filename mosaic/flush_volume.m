function flush_volume(directory, frameX, frameY,frameZ)
% Used to delete volumes containing no tissue
for x=1:frameX
    for y=1:frameY
        for z=1:frameZ
            load([directory 'vieuxvolume/volume_' sprintf( 'x%02.0f', x+12) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
            save([directory 'volume/' 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'FFTIMG');
        end
    end
end