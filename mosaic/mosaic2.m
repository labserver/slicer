function [M,m] = mosaic2(directory, frameX, frameY, frameZ, plane, pixel)
%Function to create a single mosaic image (rapid...for verification purpose)
if plane=='z'
    disp('Z plane chosen')
    load([directory 'delta.mat']);
    delta = round(delta);
    d = delta;
    delta(:,1,1,frameZ) = [0 0 0];
    load([directory 'meanVolume.mat']);
    for y = 1:frameY
        if y~=1
            delta(:,1,y,frameZ)=delta(:,1,y,frameZ)-(sum(delta(:,2:round(frameX/2),y,frameZ),2)-sum(delta(:,2:round(frameX/2),y-1,frameZ),2));
        end
        tic
        for x = 1:frameX
            fid = fopen([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', frameZ) '.mat'],'rb');
            volume = fread(fid,inf, 'uint16');
            volume = reshape(volume,[200 512 512]);
            fclose(fid);
           % volume = volume./sumV;
            
            for i = 1:size(volume,1)
                meanS(i) = sum(sum(volume(i,:,:)));
            end
            derivative=diff(meanS);
            A = find(derivative==max(derivative(1:40)));
            
            
            a = 512+sum(delta(2,1:x,y,frameZ))+sum(delta(2,1,1:(y-1),frameZ));
            b = 512+sum(delta(3,1:x,y,frameZ))+sum(delta(3,1,1:(y-1),frameZ));
            m(a+1:a+size(volume,2),b+1:b+size(volume,3)) = squeeze(volume(pixel+A,:,:));
%             m(a+1:a+size(volume,2),b+1:b+size(volume,3)) = squeeze(volume(pixel-sum(delta(1,1:x,y,frameZ))-sum(delta(1,1,1:(y-1),frameZ)),:,:));
            
            if (x==1)&&(y==1)
                M(1:size(m,1),1:size(m,2),1) = m;
                clear m
            else
                M(1:size(m,1),1:size(m,2),2) = m;
                clear m
                M(:,:,3) = sum(M,3);%./max(1,sum(M(:,:,1:2)~=0,3));
                
                if x~=1
                    L1 = repmat(linspace(0,1,512-delta(2,x,y,frameZ)),[512-delta(3,x,y,frameZ),1])';
                    L2 = repmat(linspace(1,0,512-delta(2,x,y,frameZ)),[512-delta(3,x,y,frameZ),1])';
                    M(a+1:a+(512-delta(2,x,y,frameZ)),b+1:b+ (512-delta(3,x,y,frameZ)),3)...
                        = M(a+1:a+(512-delta(2,x,y,frameZ)),b+1:b+(512-delta(3,x,y,frameZ)),1).*L2...
                        +M(a+1:a+(512-delta(2,x,y,frameZ)),b+1:b+(512-delta(3,x,y,frameZ)),2).*L1;
                end
                if y~=1
                    if x~=1
                        c=a+(512-delta(2,x,y,frameZ))+1;
                    else
                        c = max(512+1+sum(delta(2,1:x,y,frameZ))+sum(delta(2,1,1:(y-1),frameZ)),512+1+sum(delta(2,1:x,y-1,frameZ))+sum(delta(2,1,1:(y-2),frameZ)));
                    end
                    d = min(512+sum(delta(2,1:x,y,frameZ))+sum(delta(2,1,1:(y-1),frameZ))+512,512+1+sum(delta(2,1:x,y-1,frameZ))+sum(delta(2,1,1:(y-2),frameZ))+512);
                    e = 512+1+sum(delta(3,1:x,y,frameZ))+sum(delta(3,1,1:(y-1),frameZ));
                    f = 512+sum(delta(3,1:x,y-1,frameZ))+sum(delta(3,1,1:(y-2),frameZ))+512;
                    s = size( M(c:d,e:f,3));
                    L3 = repmat(linspace(0,1,s(2)),[s(1),1]);
                    L4 = repmat(linspace(1,0,s(2)),[s(1),1]);
                    
                    
            M(c:d,e:f,3) = M(c:d,e:f,1).*L4 + M(c:d,e:f,2).*L3;

                    
                end
                
                M(:,:,1) = M(:,:,3);
                M = M(:,:,1);
            end
        end
        toc
    end
    delta = d;

elseif plane=='y'
    disp('Y plane chosen')
deltaX = delta;
    for z = 1
        tic
        for x = 1:frameX
            volume = load([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', frameY) '_' sprintf( 'z%02.0f', frameZ) '.mat']);
%             a = 512+sum(delta(1,1:x,frameY,frameZ))+sum(delta(1,1,1,frameZ))+sum(delta(1,1,1:frameY,frameZ));
%             b = 512+sum(delta(2,1:x,frameY,frameZ))+sum(delta(2,1,1,frameZ))+sum(delta(2,1,1:frameY,frameZ));
              a = 512+sum(deltaX(1,1:x,frameY,frameZ));            
              b = 512+sum(deltaX(2,1:x,frameY,frameZ));
%             m(a+1:a+size(volume.fftimg,1),b+1:b+size(volume.fftimg,2))...
%                 = squeeze(volume.fftimg(:,pixel+sum(delta(3,1:x,frameY,z))+sum(delta(3,1,1,1:z))+sum(delta(3,1,1:frameY,z))-(sum(delta(3,1,frameY,1))+sum(delta(3,1,1:frameY,1))),:));
              m(a+1:a+size(volume.FFTIMG,1),b+1:b+size(volume.FFTIMG,2))...
                 = squeeze(volume.FFTIMG(:,:,pixel+sum(deltaX(3,1:x,frameY,frameZ))));
            if (x==1)
                M(1:size(m,1),1:size(m,2),1) = m;
                clear m
            else
                M(1:size(m,1),1:size(m,2),2) = m;
                clear m
                M(:,:,3) = sum(M,3);%./max(1,sum(M(:,:,1:2)~=0,3));
                c = 512+sum(deltaX(2,1:(x-1),frameY,frameZ))+512;
                l1 = size(M(:,b+1:c,3));
                L1 = repmat(linspace(1,0,l1(2)),[l1(1),1]);
                L2 = repmat(linspace(0,1,l1(2)),[l1(1),1]);
                M(:,b+1:c,3) = M(:,b+1:c,1).*L1 + M(:,b+1:c,2).*L2;
                M(:,:,1) = M(:,:,3);
                M = M(:,:,1);
            end
        end
        toc
    end

    
    
elseif plane=='x'
    disp('X plane chosen')
    deltaY = delta;
        for z = 1
        tic
        for y = 1:frameY
            volume = load([directory 'volume_' sprintf( 'x%02.0f', frameX) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
            a = 512+sum(deltaY(1,frameX,1:y,frameZ));
            b = 512+sum(deltaY(3,frameX,1:y,frameZ));
            
%             m(a+1:a+size(volume.fftimg,1),b+1:b+size(volume.fftimg,2))...
%                 = squeeze(volume.fftimg(:,pixel+sum(delta(3,1:x,frameY,z))+sum(delta(3,1,1,1:z))+sum(delta(3,1,1:frameY,z))-(sum(delta(3,1,frameY,1))+sum(delta(3,1,1:frameY,1))),:));
              m(a+1:a+size(volume.FFTIMG,1),b+1:b+size(volume.FFTIMG,2))...
                 = squeeze(volume.FFTIMG(:,pixel+sum(deltaY(2,frameX,1:y,frameZ)),:));
            if (y==1)
                M(1:size(m,1),1:size(m,2),1) = m;
                clear m
            else
                M(1:size(m,1),1:size(m,2),2) = m;
                clear m
                M(:,:,3) = sum(M,3);%./max(1,sum(M(:,:,1:2)~=0,3));
                c = 512+sum(deltaY(3,frameX,1:(y-1),z))+512;
                l1 = size(M(:,b+1:c,3));
                L1 = repmat(linspace(1,0,l1(2)),[l1(1),1]);
                L2 = repmat(linspace(0,1,l1(2)),[l1(1),1]);
                M(:,b+1:c,3) = M(:,b+1:c,1).*L1 + M(:,b+1:c,2).*L2;
                M(:,:,1) = M(:,:,3);
                M = M(:,:,1);
            end
        end
        toc
    end
end