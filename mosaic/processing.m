function processing(directory, frameX, frameY, frameZ)
%processing of raw data

%Reference
fid=fopen([directory '\reference.bin'],'rb');
ref = fread(fid, inf, 'uint16');
ref = reshape(ref,[1152 512]);

% Filter
filter = blackman(4*1152,'symmetric');
filter = filter(end/2-575:end/2+576);
for z = 1:frameZ
    for y = 1:frameY
        for x = 1:frameX
            tic
            RD = load([directory '\RawData_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
            RD.img = RD.img - repmat(mean(ref,2), [1 512 512]);
            RD.img = RD.img.*repmat(filter,[1 512 512]);
            fftimg = fft(RD.img,[],1);
            clear RD.img
            %fftimg = 10*log10(abs(fftimg(1:150,:,:))); % on garde les donn�es significatives pour r�duire la taille
            fftimg = (abs(fftimg(1:150,:,:)));
            noise_floor = mean(mean(fftimg(round((1-0.05)*end):end,:)));
            fftimg = 10*log10(fftimg / noise_floor);
            fftimg(fftimg<0) = 0;
            save([directory '\volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'fftimg');
            clear fftimg
            toc
        end
    end
end