function [deltaX, deltaY, deltaZ] = shift3d(directory, frameX, frameY,frameZ)
% Donne le décallage entre les volumes dans une dimension donnée

% deltaX = zeros(3,frameX,frameY,frameZ);
% deltaY = zeros(3,frameX,frameY,frameZ);
% delta = zeros(3,frameX,frameY,frameZ);

% for z = 1:frameZ
%     tic
%     for y = 1:frameY
%         for x = 1: frameX-1
%             if exist('VolumePost');
%                 VolumePre = VolumePost;
%                 clear VolumePost;
%             else
%                 VolumePre = load([directory '\volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
%             end
%             % deuxieme volume
%             VolumePost = load([directory '\volume_' sprintf( 'x%02.0f', x+1) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
%             %Calcul du decallage
%             [deltax , deltay, deltaz] = ExtPhaseCorrelation3D(VolumePre.fftimg, VolumePost.fftimg);
%             deltaX(:,x+1,y,z) =  [deltaz deltax deltay];
%         end
%         clear VolumePost
%     end
%     toc
% end
% 
% for z = 1:frameZ
%     tic
%     for x = 1:frameX
%         for y = 1:frameY-1
%             if exist('VolumePost');
%                 VolumePre = VolumePost;
%                 clear VolumePost;
%             else
%                 VolumePre = load([directory '\volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
%             end
%             % deuxieme volume
%             VolumePost = load([directory '\volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y+1) '_' sprintf( 'z%02.0f', z) '.mat']);
%             %Calcul du decallage
%             [deltax , deltay, deltaz] = ExtPhaseCorrelation3D(VolumePre.fftimg, VolumePost.fftimg);
%             deltaY(:,x,y+1,z) =  [deltaz deltax deltay];
%         end
%         clear VolumePost
%     end
%     toc
% end

for y = 3:frameY
    tic
    for x = 4: frameX
        for z = 1:frameZ-1
            if exist('VolumePost');
                VolumePre = VolumePost;
                clear VolumePost;
            else
                VolumePre = load([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat']);
%                 for i = 1:size(VolumePre.FFTIMG,2)
%                     for j = 1: size(VolumePre.FFTIMG,3)
%                         a(i,j) = find(VolumePre.FFTIMG(:,i,j)==max(VolumePre.FFTIMG(:,i,j)));
%                     end
%                 end
%                 meanA = round(mean(mean(a)));
%                 clear a
                 VolumePre.FFTIMG = VolumePre.FFTIMG(21:70,:,:);
            end
            % deuxieme volume
            VolumePost = load([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z+1) '.mat']);
%             for i = 1:size(VolumePost.FFTIMG,2)
%                 for j = 1: size(VolumePost.FFTIMG,3)
%                     a(i,j) = find(VolumePost.FFTIMG(:,i,j)==max(VolumePost.FFTIMG(:,i,j)));
%                 end
%             end
%             meanA = round(mean(mean(a)));
%             clear a
             VolumePost.FFTIMG = VolumePost.FFTIMG(21:70,:,:);
            %Calcul du decallage
            [deltaX , deltaY, deltaZ] = ExtPhaseCorrelation3D(VolumePre.FFTIMG, VolumePost.FFTIMG);
            delta(:,x,y,z+1) =  [deltaZ deltaX deltaY];
        end
        clear VolumePost
    end
    toc
end

% deltaX = round(deltaX);
% deltaY = round(deltaY);
delta = round(delta);

%Ramener a zero les fauts deplacements
% for z = 1:size(deltaX,4)
%     for y = 1:size(deltaX,3)
%         for x = 1: size(deltaX,2)
%             if sum(deltaX(:,x,y,z),1) < 10
%                 deltaX(:,x,y,z) = 0;
%             end
%         end
%     end
% end
% for z = 1:size(deltaY,4)
%     for y = 1:size(deltaY,3)
%         for x = 1: size(deltaY,2)
%             if sum(deltaY(:,x,y,z),1) < 10
%                 deltaY(:,x,y,z) = 0;
%             end
%         end
%     end
% end
% for z = 1:size(deltaZ,4)
%     for y = 1:size(deltaZ,3)
%         for x = 1: size(deltaZ,2)
%             if sum(deltaZ(:,x,y,z),1) < 10
%                 deltaZ(:,x,y,z) = 0;
%             end
%         end
%     end
% end


% for z = 1:size(deltaZ,4)
%     for y = 1:size(deltaZ,3)
%         for x = 1: size(deltaZ,2)
%             if (z~=1)&&(sum(deltaZ(:,x,y,z),1) < 10)
%                 deltaZ(1,x,y,z) = 30;
%                 deltaZ(2,x,y,z) = 0;
%                 deltaZ(3,x,y,z) = 0;
%             end
%         end
%     end
% end
% for z = 1:size(deltaY,4)
%     for y = 1:size(deltaY,3)
%         for x = 1: size(deltaY,2)
%             if (y~=1)&&(sum(deltaY(:,x,y,z),1) < 100)
%                 deltaY(1,x,y,z) = sum(sum(sum(deltaY(1,:,2:end,:))))/length(find(deltaY(1,:,2:end,:)));
%                 deltaY(2,x,y,z) = sum(sum(sum(deltaY(2,:,2:end,:))))/length(find(deltaY(2,:,2:end,:)));
%                 deltaY(3,x,y,z) = sum(sum(sum(deltaY(3,:,2:end,:))))/length(find(deltaY(3,:,2:end,:)));
%             end
%         end
%     end
% end
% 
% for z = 1:size(deltaX,4)
%     for y = 1:size(deltaX,3)
%         for x = 1: size(deltaX,2)
%             if (x~=1)&&(sum(deltaX(:,x,y,z),1) < 10)
%                 deltaX(1,x,y,z) = sum(sum(sum(deltaX(1,2:end,:,:))))/length(find(deltaX(1,2:end,:,:)));
%                 deltaX(2,x,y,z) = sum(sum(sum(deltaX(2,2:end,:,:))))/length(find(deltaX(2,2:end,:,:)));
%                 deltaX(3,x,y,z) = sum(sum(sum(deltaX(3,2:end,:,:))))/length(find(deltaX(3,2:end,:,:)));
%                 
%             end
%         end
%     end
% end
% deltaX = round(deltaX);
% deltaY = round(deltaY);


% save([directory '\deltaX.mat'],'deltaX');
% save([directory '\deltaY.mat'],'deltaY');
save([directory 'deltaZ.mat'],'delta');
