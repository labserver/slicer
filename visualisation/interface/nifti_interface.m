function varargout = nifti_interface(varargin)
% NIFTI_INTERFACE MATLAB code for nifti_interface.fig
%      NIFTI_INTERFACE, by itself, creates a new NIFTI_INTERFACE or raises the existing
%      singleton*.
%
%      H = NIFTI_INTERFACE returns the handle to a new NIFTI_INTERFACE or the handle to
%      the existing singleton*.
%
%      NIFTI_INTERFACE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NIFTI_INTERFACE.M with the given input arguments.
%
%      NIFTI_INTERFACE('Property','Value',...) creates a new NIFTI_INTERFACE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before nifti_interface_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to nifti_interface_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help nifti_interface

% Last Modified by GUIDE v2.5 13-Jan-2014 15:59:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @nifti_interface_OpeningFcn, ...
                   'gui_OutputFcn',  @nifti_interface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before nifti_interface is made visible.
function nifti_interface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to nifti_interface (see VARARGIN)

% Choose default command line output for nifti_interface
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes nifti_interface wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%slider1
set(handles.sCoronal,'Max',512)
set(handles.sCoronal,'Min',1)
set(handles.sCoronal, 'Value', 256);
set(handles.sCoronal, 'SliderStep', [1/511 1/511]);
%slider2
set(handles.sTransversal,'Max',512)
set(handles.sTransversal,'Min',1)
set(handles.sTransversal, 'Value', 256);
set(handles.sTransversal, 'SliderStep', [1/511 1/511]);
%slider3
set(handles.sSagittal,'Max',512)
set(handles.sSagittal,'Min',1)
set(handles.sSagittal, 'Value', 256);
set(handles.sSagittal, 'SliderStep', [1/511 1/511]);

   
    % Verify if reduced version is already computed
    nifti=load_nii('/home/vibratome/pouf.nii');
  %  if(size(tmp,1)==0)
        % Need to do the image subsampling (careful with ordering)
        tmp_volume=zeros(512,512,282);
        for i=1:282
            big_img=squeeze(nifti.img(:,:,i));
            tmp_volume(:,:,i) = imresize(big_img,[512,512]);
        end
        % We reduced in x and y, need to reduce in z...
        volume=zeros(512,512,512);
        for i=1:512
            volume(i,:,:)=imresize(squeeze(tmp_volume(i,:,:)),[512,512]);
        end
        clear tmp_volume;
        %save([handles.folder '/vol_reduced.mat'],'volume');
%     else
%         % Load Reduced version
%         load([handles.folder '/vol_reduced.mat']);
    
    handles.volume=volume;
    handles.bs = 0;
    % Update display
    axes(handles.aTransversal)
    k=rot90(squeeze(volume(:,:,end/2)));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    
    axis off;
    f =get(handles.aTransversal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,2})
    axes(handles.aCoronal)
    k=rot90(squeeze(volume(:,end/2,:)));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    
    axis off;
    f =get(handles.aCoronal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,1})
    axes(handles.aSagittal)
    k=fliplr(rot90(squeeze(volume(end/2,:,:))));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    axis off;
    f =get(handles.aSagittal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
    
    % Update handles structure
    guidata(hObject, handles);




% --- Outputs from this function are returned to the command line.
function varargout = nifti_interface_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
load('/home/vibratome/Dropbox/OCT+Vibratome/OCT_vibratome/NIFTI/interface/brain_structure.mat')
value=get(hObject,'Value');
string=cellstr(get(hObject,'String'));
a=char(string(value));
handles.bs=getfield(BS,char(a));

% Update display
    axes(handles.aTransversal)
    k=rot90(squeeze(handles.volume(:,:,512-round(get(handles.sTransversal,'Value')))));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    
    axis off;
    f =get(handles.aTransversal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,2})
    axes(handles.aCoronal)
    k=rot90(squeeze(handles.volume(:,512-round(get(handles.sCoronal,'Value')),:)));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    
    axis off;
    f =get(handles.aCoronal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,1})
    axes(handles.aSagittal)
    k=fliplr(rot90(squeeze(handles.volume(round(get(handles.sSagittal,'Value')),:,:))));
    imagesc(k); colormap 'gray'
    set(gca,'DataAspectRatio',[1 1 1])
    a= red_area(k,handles.bs);
    red=cat(3,ones(size(a)),zeros(size(a)),zeros(size(a)));
    hold on
    r=imshow(red);
    hold off
    set(r,'AlphaData',a);
    axis off;
    f =get(handles.aSagittal,'Children');
    set(f,'ButtonDownFcn',{@getpoints,handles,3})
    
    % Update handles structure
    guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function getpoints(src,evnt,handles,which_axe)

switch(which_axe)
    
    case 1
        
        cp = get(handles.aCoronal,'CurrentPoint');
        axes(handles.aCoronal)
        imagesc(rot90(squeeze(handles.volume(:,512-round(get(handles.sCoronal,'Value')),:))));
        set(gca,'DataAspectRatio',[1 1 1])
        line([1:512],cp(3), 'Color', [1 0 0])
        line(cp(1),[1:512], 'Color', [1 0 0])
        axis off;
        f =get(handles.aCoronal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,1})
        axes(handles.aTransversal)
        imagesc(rot90(squeeze(handles.volume(:,:,round(512-cp(3)),:))));
        set(gca,'DataAspectRatio',[1 1 1])
        line(cp(1),[1:512], 'Color', [1 0 0])
        line([1:512],round(get(handles.sCoronal,'Value')), 'Color', [1 0 0])
        axis off;
        f =get(handles.aTransversal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,2})
        axes(handles.aSagittal)
        imagesc(fliplr(rot90(squeeze(handles.volume(round(cp(1)),:,:)))));
        set(gca,'DataAspectRatio',[1 1 1])
        line([1:512],cp(3), 'Color', [1 0 0])
        line(round(get(handles.sCoronal,'Value')),[1:512], 'Color', [1 0 0])
        axis off;
        f =get(handles.aSagittal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,3})
        set(handles.sTransversal, 'Value', round(cp(3)));
        set(handles.tTransversal,'String', num2str(round(cp(3))));
        set(handles.sSagittal, 'Value', round(cp(1)));
        set(handles.tSagittal,'String', num2str(round(cp(1))));

        
                
        
    case 2
        cp = get(handles.aTransversal,'CurrentPoint');
        axes(handles.aTransversal)
        imagesc(rot90(squeeze(handles.volume(:,:,512-round(get(handles.sTransversal,'Value'))))));
        set(gca,'DataAspectRatio',[1 1 1])
        line([1:512],cp(3), 'Color', [1 0 0])
        line(cp(1),[1:512], 'Color', [1 0 0])
        axis off;
        f =get(handles.aTransversal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,2})
        axes(handles.aCoronal)
        imagesc(rot90(squeeze(handles.volume(:,round(512-cp(3)),:))));
        set(gca,'DataAspectRatio',[1 1 1])
        line(cp(1),[1:512], 'Color', [1 0 0])
        line([1:512],round(get(handles.sTransversal,'Value')), 'Color', [1 0 0])
        axis off;
        f =get(handles.aCoronal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,1})
        axes(handles.aSagittal)
        imagesc(fliplr(rot90(squeeze(handles.volume(round(cp(1)),:,:)))));
        set(gca,'DataAspectRatio',[1 1 1])
        line([1:512],round(get(handles.sTransversal,'Value')), 'Color', [1 0 0])
        line(cp(3),[1:512], 'Color', [1 0 0])
        axis off;
        f =get(handles.aSagittal,'Children');
         set(f,'ButtonDownFcn',{@getpoints,handles,3})
        set(handles.sCoronal, 'Value', round(cp(3)));
        set(handles.tCoronal,'String', num2str(round(cp(3))));
        set(handles.sSagittal, 'Value', round(cp(1)));
        set(handles.tSagittal,'String', num2str(round(cp(1))));
        
        
    case 3
        cp = get(handles.aSagittal,'CurrentPoint');
        axes(handles.aSagittal)
        imagesc(fliplr(rot90(squeeze(handles.volume(round(get(handles.sSagittal,'Value')),:,:)))));
        set(gca,'DataAspectRatio',[1 1 1])
        line([1:512],cp(3), 'Color', [1 0 0])
        line(cp(1),[1:512], 'Color', [1 0 0])
        axis off;
        f =get(handles.aSagittal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,3})
        axes(handles.aTransversal)
        imagesc(rot90(squeeze(handles.volume(:,:,round(512-cp(3))))));
        set(gca,'DataAspectRatio',[1 1 1])
        line(round(get(handles.sSagittal,'Value')),[1:512], 'Color', [1 0 0])
        line([1:512],cp(1), 'Color', [1 0 0])
        axis off;
        f =get(handles.aTransversal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,2})
        axes(handles.aCoronal)
        imagesc(rot90(squeeze(handles.volume(:,512-round(cp(1)),:))));
        set(gca,'DataAspectRatio',[1 1 1])
        line(round(get(handles.sSagittal,'Value')),[1:512], 'Color', [1 0 0])
        line([1:512],cp(3), 'Color', [1 0 0])
        axis off;
        f =get(handles.aCoronal,'Children');
        set(f,'ButtonDownFcn',{@getpoints,handles,1})
        set(handles.sCoronal, 'Value', round(cp(1)));
        set(handles.tCoronal,'String', num2str(round(cp(1))));
        set(handles.sTransversal, 'Value', round(cp(3)));
        set(handles.tTransversal,'String', num2str(round(cp(3))));
end



% --- Executes on slider movement.
function sCoronal_Callback(hObject, eventdata, handles)
% hObject    handle to sCoronal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


set(handles.tCoronal,'String', num2str(round(get(handles.sCoronal,'Value'))));
axes(handles.aCoronal)
imagesc(rot90(squeeze(handles.volume(:,512-round(get(handles.sCoronal,'Value')),:))));
set(gca,'DataAspectRatio',[1 1 1])
axis off;
f =get(handles.aCoronal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})
axes(handles.aTransversal)
imagesc(rot90(squeeze(handles.volume(:,:,512-round(get(handles.sTransversal,'Value'))))));
set(gca,'DataAspectRatio',[1 1 1])
line([1:512],round(get(handles.sCoronal,'Value')), 'Color', [1 0 0])
axis off;
f =get(handles.aTransversal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})
axes(handles.aSagittal)
imagesc(fliplr(rot90(squeeze(handles.volume(round(get(handles.sSagittal,'Value')),:,:)))));
set(gca,'DataAspectRatio',[1 1 1])
line(round(get(handles.sCoronal,'Value')),[1:512], 'Color', [1 0 0])
axis off;
f =get(handles.aSagittal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})


% --- Executes during object creation, after setting all properties.
function sCoronal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sCoronal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sTransversal_Callback(hObject, eventdata, handles)
% hObject    handle to sTransversal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

set(handles.tTransversal,'String', num2str(round(get(handles.sTransversal,'Value'))));
axes(handles.aTransversal)
imagesc(rot90(squeeze(handles.volume(:,:,round(512-get(handles.sTransversal,'Value'))))));
set(gca,'DataAspectRatio',[1 1 1])
axis off;
f =get(handles.aTransversal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})
axes(handles.aCoronal)
imagesc(rot90(squeeze(handles.volume(:,512-round(get(handles.sCoronal,'Value')),:))));
set(gca,'DataAspectRatio',[1 1 1])
line([1:512],round(get(handles.sTransversal,'Value')), 'Color', [1 0 0])
axis off;
f =get(handles.aCoronal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})
axes(handles.aSagittal)
imagesc(fliplr(rot90(squeeze(handles.volume(round(get(handles.sSagittal,'Value')),:,:)))));
set(gca,'DataAspectRatio',[1 1 1])
line([1:512],round(get(handles.sTransversal,'Value')), 'Color', [1 0 0])
axis off;
f =get(handles.aSagittal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})



% --- Executes during object creation, after setting all properties.
function sTransversal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sTransversal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sSagittal_Callback(hObject, eventdata, handles)
% hObject    handle to sSagittal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.tSagittal,'String', num2str(round(get(handles.sSagittal,'Value'))));
axes(handles.aSagittal)
imagesc(fliplr(rot90(squeeze(handles.volume(round(get(handles.sSagittal,'Value')),:,:)))));
set(gca,'DataAspectRatio',[1 1 1])
axis off;
f =get(handles.aSagittal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,3})
axes(handles.aCoronal)
imagesc(rot90(squeeze(handles.volume(:,512-round(get(handles.sCoronal,'Value')),:))));
set(gca,'DataAspectRatio',[1 1 1])
line(round(get(handles.sSagittal,'Value')),[1:512], 'Color', [1 0 0])
axis off;
f =get(handles.aCoronal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,1})
axes(handles.aTransversal)
imagesc(rot90(squeeze(handles.volume(:,:,512-round(get(handles.sTransversal,'Value'))))));
set(gca,'DataAspectRatio',[1 1 1])
line(round(get(handles.sSagittal,'Value')),[1:512], 'Color', [1 0 0])
axis off;
f =get(handles.aTransversal,'Children');
set(f,'ButtonDownFcn',{@getpoints,handles,2})



% --- Executes during object creation, after setting all properties.
function sSagittal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sSagittal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
