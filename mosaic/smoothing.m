%% Smoothing en z

%% Acquisition de la matrice 3D � traiter

% Fichier original de taille 3381x4817x800 (pr�cision : unit16)

% size.x = 3381;
% size.y = 4871;
% size.z = 800;
% 
% % Ouverture du memmapfile du volume 3D
% 
% m=memmapfile('F:\Projet GBM3100\MemMapFile.mat', 'Format', {'uint16' [size.x size.y size.z] 'x'}, 'Writable', true);

%% Tailles
function smoothing(directory, size_x, size_y, size_z)
% N nombre de points sur chaque A-line
% i est l'indice de la slice en question
% ai et bi moyennes locales d'intensit�s au d�but et fin des A-lines pour
% la slice i
% Principe : smoothing en conservant les intensit�s au centre mais en les
% diminuant progressivement aux bords

% size_x = 3381;
% size_y = 4871;
% size_z = 800;   % CHANGER

m=memmapfile([directory 'MemMapFile.mat'], 'Format', {'uint16' [size_x size_y size_z] 'x'}, 'Writable', true);

nbr_pixel_z = 40;               % Nombre de pixels par slice en z
nbr_slice = size_z/nbr_pixel_z; % Nombre de slices au total

%% Calcul des coefficients a(n) et b(n)

% Initialisations de matrices

a=ones(1,nbr_slice);            % Matrice des moyennes des couches au d�but des slices
b=ones(1,nbr_slice);            % Matrice des moyennes des couches � la fin des slices
L=ones(nbr_slice,nbr_pixel_z);  % Matrice des coeff. de r�gulation L(z)
N=(20:nbr_pixel_z:size_z);      % Matrice des positions z milieu de chaque slice


% Remplir matrices des coeff. a et b

for n = 1:nbr_slice                                     % Parcourir les slices
            
            A = m.Data(1).x(:,:,(1+(n-1)*nbr_pixel_z)); % Matrice provisoire de la 1ere couche de chaque slice
            a(n) = mean2(A);                            % Moyenne
            
            B = m.Data(1).x(:,:,(n*nbr_pixel_z));       % Matrice provisoire de la 40e (derni�re) couche de chaque slice
            b(n) = mean2(B);                            % Moyenne
                    
end

% Calcul des coefficients L(z)

for j=1:nbr_slice                   % Pour chaque slice
    for k=1:nbr_pixel_z             % Pour chaque couche de la slice (40 pixels)      
            
        if j==1                     % Pour la condition limite de la slice 1 (b(j-1)=b(0) non d�fini)
            
            if k<=N(j)
            L(j,k) = exp((-(2*(((j-1)*nbr_pixel_z)+k)-N(j))/N(j))*log((a(j)/2*a(j)))); % La position z est donn�e par (j-1)*nbr_pixel_z  + k
            else
            L(j,k) = exp(((2*(((j-1)*nbr_pixel_z)+k)-N(j))/N(j))*log((a(j+1)+b(j))/2*b(j))); 
            end
            
        
        
        elseif j==nbr_slice                % Pour la condition limite de la slice 20 (a(j+1) = a(21) non d�fini)
            %%% CHANGER 20 POUR nbr_slice
            if k<=N(j)
            L(j,k) = exp((-(2*(((j-1)*nbr_pixel_z)+k)-N(j))/N(j))*log((a(j)+b(j-1))/2*a(j))); 
            else
            L(j,k) = exp(((2*(((j-1)*nbr_pixel_z)+k)-N(j))/N(j))*log((b(j))/2*b(j))); 
            end
            
        
        
                                    % Pour les slices 2 � 19
       
        else
            if k<=N(j)
            L(j,k) = exp((-(2*(((j-1)*nbr_pixel_z)+k)-N(j))/N(j))*log((a(j)+b(j-1))/2*a(j))); 
            else
            L(j,k) = exp(((2*(((j-1)*nbr_pixel_z)+k)-N(j))/N(j))*log((a(j+1)+b(j))/2*b(j))); 
            end
        
        end 
    end
end

% Pour passer de L matrice 2D � L vecteur 1D contenant les coeff. L(z) de
% la couche 1 � 800

L=L';
L=reshape(L,size_z,1);


F = zeros(size_x, size_y, 1);

% Enregistrement couche par couche
for pix=1:size_z
        
        if pix==1   % 1er enregistrement
            
            F(:,:,1) = m.Data(1).x(:,:,pix).*L(pix);
            F = uint16(F);
            fid = fopen([directory 'arte.mat'], 'w');
            fwrite(fid,F, 'uint16');
        
        else        % enregistrements subs�quents
        
            F(:,:,1) = m.Data(1).x(:,:,pix).*L(pix);
            F = uint16(F);
            fid = fopen([directory 'arte.mat'], 'a');
            fwrite(fid,F, 'uint16');
            
        end
        
        
end

end

%-------------------------------------------------------------------------%
%
% Voir d'autres moyens ''classiques'' pour r�duire l'art�fact et comparer
% art�fact pr�sent entre z = 40 et z = 41 --- interpoler I en 38 � 42 �
% l'aide des points 37 et 43










% F = zeros(size_x, size_y,nbr_pixel_z);
% 
% for slice=1:nbr_slice
%     for pix=1:nbr_pixel_z
%         
%         if slice==1
%             
%             F(:,:,(1+(slice-1)*nbr_pixel_z):(slice*nbr_pixel_z)) = m.Data(1).x(:,:,(1+(slice-1)*nbr_pixel_z):(slice*nbr_pixel_z)).*L(slice,pix);
%             F = uint16(F);
%             fid = fopen('E:\Projet GBM3100\FINAL.mat', 'w');
%             fwrite(fid,F, 'uint16');
%         
%         else
%         
%         %F(:,:,(1+(slice-1)*nbr_pixel_z)) = (m.Data(1).x(:,:,(1+(slice-1)*nbr_pixel_z))).*L(slice,pix);
%         
%             F(:,:,(1+(slice-1)*nbr_pixel_z):(slice*nbr_pixel_z)) = m.Data(1).x(:,:,(1+(slice-1)*nbr_pixel_z):(slice*nbr_pixel_z)).*L(slice,pix);
%             F = uint16(F);
%             fid = fopen('E:\Projet GBM3100\FINAL.mat', 'a');
%             fwrite(fid,F, 'uint16');
%             
%         end
%         
%         
%     end
%     
% %     
% %      fid = fopen(['F:\Projet GBM3100\Smooth.mat'], 'w');  
% %             fwrite(fid,F, 'uint16');
% end




            
            
            
            