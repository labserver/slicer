function [sumV] = meanVolume (directory, frameX, frameY, frameZ)
fid = fopen([directory 'volume_' sprintf( 'x%02.0f', 1) '_' sprintf( 'y%02.0f', 1) '_' sprintf( 'z%02.0f', 1) '.mat'],'rb');
volume = fread(fid,inf, 'uint16');
volume = reshape(volume,[200 512 512]);
fclose(fid);
sumV = zeros(size(volume));
clear volume
for z = 1:frameZ
    for y = 1:frameY
        tic
        for x = 1:frameX
            tic
            fid = fopen([directory 'volume_' sprintf( 'x%02.0f', x) '_' sprintf( 'y%02.0f', y) '_' sprintf( 'z%02.0f', z) '.mat'],'rb');
            volume = fread(fid,inf, 'uint16');
            volume = reshape(volume,[200 512 512]);
            fclose(fid);
            F = (x-1) + frameX*(y-1) + frameX*frameY*(z-1);
            sumV = (F*(sumV)+volume)/(F+1);
            clear volume
            toc
        end
        toc
    end
end
save([directory 'meanVolume.mat'],'sumV');

