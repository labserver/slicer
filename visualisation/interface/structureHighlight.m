function [a] = structureHighlight(nifti,bs)
a=zeros(size(nifti));
a(nifti==bs)=1;


% for k=1:size(nifti,3)
% for i=1:size(nifti,1)
%     for j=1:size(nifti,2)
%         if nifti(i,j,k)==bs
%             a(i,j,k)=1;
%         else
%             a(i,j,k)=0;
%         end
%     end
% end
% end